﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;


    private void Awake()
    {
        if (instance != null)
        {

            Debug.LogError("More than 1 Instance of GameManager");
            return;
        }

        instance = this;
        Time.timeScale = 1f;
    }


    [Header("Player")]
    public PlayerStats PlayerDefaultStats;
    public int playerCurrency;
    private int playerHealth;
    private int Damage;
    private int playerScore = 0;

    [Header("UI")]
    public Text moneyText;
    public Text healthText;

    public GameObject loseScreen;
    public GameObject winScreen;
    public GameObject disableGO;

    private bool tutorial = false;

    [HideInInspector]
    public int turretCount = 0;

    private GetAudioSource audio;
    private PauseScreen pauseScreen;
    private Animator camAnim;

    private bool won = false;
    private bool lost = false;

    private void Start()
    {
        camAnim = Camera.main.gameObject.GetComponent<Animator>();
        camAnim.enabled = false;
        pauseScreen = FindObjectOfType<PauseScreen>();
        audio = GetComponent<GetAudioSource>();
        playerCurrency = PlayerDefaultStats.startingAmount;
        playerHealth = PlayerDefaultStats.maxHealth;
    }

    private void Update()
    {
        moneyText.text = " " + playerCurrency;
        healthText.text = " " + playerHealth;
        if(playerHealth <= 0)
        {
            Debug.Log("Game Over!!");
            LoseGame();
        }

        if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            audio.PlaySound(1);
        }
    }

    public void GetBling()
    {
        audio.PlaySound(0);
    }

    public void DamagePlayer(int damage)
    {
        playerHealth -= damage;
    }

    public void UseMoney(int cost)
    {
        playerCurrency -= cost;

        if (tutorial)
        {
            TutorialManager tutorialManager = FindObjectOfType<TutorialManager>();
            tutorialManager.TriggerTutorial();
            tutorial = false;
        }
    }

    public void GetMoney(int value)
    {
        playerCurrency += value;
    }

    public void AddScore(int scoreToGive)
    {
        playerScore += scoreToGive;
    }

    public bool MoneyCheck(int turretCost)
    {
        if(playerCurrency >= turretCost)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void TutorialCheck()
    {
        tutorial = true;
    }

    public void TurretCount(int count)
    {
        turretCount += count;
    }
    
    public void WinGame()
    {
        if (!won)
        {
            StartCoroutine(WinRoutine());
            won = true;

            if(disableGO != null)
            {
                disableGO.SetActive(false);
            }
        }
    }

    IEnumerator WinRoutine()
    {
        audio.PlaySound(4);
        pauseScreen.TogglePause();
        pauseScreen.thePauseScreen.SetActive(false);
        camAnim.enabled = true;
        Time.timeScale = 1.0f;
        yield return new WaitForSeconds(2);

        winScreen.SetActive(true);
        camAnim.SetBool("Ended", true);

        int stars = 0;

        if (playerHealth / PlayerDefaultStats.maxHealth >= 0)
        {
            stars = 0;
        }
        if (playerHealth / PlayerDefaultStats.maxHealth >= 1 / 5)
        {
            stars = 1;
        }
        if (playerHealth / PlayerDefaultStats.maxHealth >= 2 / 5)
        {
            stars = 2;
        }
        if (playerHealth / PlayerDefaultStats.maxHealth >= 3 / 5)
        {
            stars = 3;
        }
        if (playerHealth / PlayerDefaultStats.maxHealth >= 4 / 5)
        {
            stars = 4;
        }
        if (playerHealth / PlayerDefaultStats.maxHealth == 1)
        {
            stars = 5;
        }

        Animator starAnim = winScreen.GetComponent<Animator>();
        for (int i = 0; i < stars + 1; i++)
        {
            starAnim.SetInteger("Stars", i);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void LoseGame()
    {
        if (!lost)
        {
            StartCoroutine(LoseRoutine());
            lost = true;
            if (disableGO != null)
            {
                disableGO.SetActive(false);
            }
        }
    }

    IEnumerator LoseRoutine()
    {
        audio.PlaySound(3);
        pauseScreen.TogglePause();
        pauseScreen.thePauseScreen.SetActive(false);
        camAnim.enabled = true;
        Time.timeScale = 1.0f;
        yield return new WaitForSeconds(2);

        loseScreen.SetActive(true);

        camAnim.SetBool("Ended", true);

    }
}
