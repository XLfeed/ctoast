﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float startSpeed = 10f;
    private float currentSpeed;

    public int damage = 1;

    public float pointOffset = 0.2f;

    private Transform target;
    public int waypointIndex = 0;
    public float distanceToNextWaypoint;
    public float distanceTravelled;
    public float timePassed;

    // Start is called before the first frame update
    void Start()
    {
        currentSpeed = startSpeed;
        target = EnemyPath.waypoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        distanceToNextWaypoint = Vector3.Distance(transform.position, target.transform.position);
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * currentSpeed * Time.deltaTime, Space.World);

        if (distanceToNextWaypoint <= pointOffset)
        {

            GetNextWaypoint();
        }
        timePassed += Time.deltaTime;
        distanceTravelled = currentSpeed * Time.deltaTime * timePassed;
    }

    void GetNextWaypoint()
    {
        if(waypointIndex >= EnemyPath.waypoints.Length - 1)
        {
            //calls function to decrease health before destroying
            GameManager.instance.DamagePlayer(damage);
            WaveSpawner.enemyCount--;
            Destroy(gameObject);
            return;
        }


        waypointIndex++;
        target = EnemyPath.waypoints[waypointIndex];
    }
    public void Slow(float slowFactor)
    {
        currentSpeed = startSpeed * slowFactor;
    }
    public void Recover()
    {
        currentSpeed = startSpeed;
    }
}
