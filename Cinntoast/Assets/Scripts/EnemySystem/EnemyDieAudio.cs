﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDieAudio : MonoBehaviour
{
    public GetAudioSource gettingAS;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Enemy") || other.tag == ("FlyingEnemy"))
        {
            gettingAS.PlaySound(0);
        }
    }
}
