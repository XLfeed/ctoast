﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingWayPoints : MonoBehaviour
{

    [Header("Points")]
    public Transform[] points;

    void Start()
    {
        points = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            points[i] = transform.GetChild(i);
        }
    }
}
