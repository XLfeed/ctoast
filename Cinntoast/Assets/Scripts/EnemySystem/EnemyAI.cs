﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]

public class EnemyAI : MonoBehaviour
{
    NavMeshAgent agent;
    Enemy enemyScript;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        enemyScript = GetComponent<Enemy>();
    }

    void Update()
    {
        if (enemyScript.flying)
        {
            agent.SetDestination(GameObject.Find("FlyEndPoint").transform.position);
        }
        else
        {
            agent.SetDestination(GameObject.Find("EndPoint").transform.position);
        }
    }
}
