﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Header("Health")]
    public float enemyMaxHealth;
    private float enemyHealth;
    private float enemyLerpHealth;
    public Image enemyHealthBar;   // public Slider healthSlider;
    public Image enemyDamageIndicator;
    public float decreaseRate = 0.1f;
    public float waveCounter;

    [Header("Damage")]
    public int damage;

    [Header("Currency")]
    public int value;

    [Header("Properties")]
    public float getDamageFill = 0;
    public float getKillFill = 0;
    private bool slowed = false;
    private bool burning = false;
    private float nextBurn;
    private float burnInterval;
    private float burnDamage;
    private float burnTime;
    private float originalSpeed;
    public float healthScale;
    
    public ParticleSystem burnParticles;
    public ParticleSystem slowParticles;

    [Header("Enemy Types")]
    public bool flying = false;
    public bool cheese = false;

    private NavMeshAgent agent;

    [Header("Turret Upgrade Bar")]
    private Turret Turrettargetingthis;

    [Header("Score")]
    public int scoreToGive;

    [Header("Animation")]
    public Animator currentAnim;
    public bool idle = false;

    [Header("Cheese")]
    public GameObject[] cheeseMeshes;
    public float cheeseSpeed = 0.5f;
    public float speedIncrease = 0.5f;

    [HideInInspector]
    public float distanceTravelled = 0;

    public GameObject cookedSound;

    private bool paused;
    private float prePauseSpeed;

    private PauseScreen pauseScreen;
    private bool died;

    private void Awake()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;

        slowParticles.Stop();
        burnParticles.Stop();
    }

    public void GetDamage(float damage)
    {
        enemyHealth -= damage;
    }

    private void Start()
    {
        enemyMaxHealth = healthScale * (WaveSpawner.waveNumber * WaveSpawner.waveNumber) + enemyMaxHealth; //Health Multiplier
        enemyHealth = enemyMaxHealth;

        agent = GetComponent<NavMeshAgent>();
        originalSpeed = agent.speed;

        if (cheese)
        {
            agent.speed = cheeseSpeed;
        }
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
        if (!paused && agent)
        {
            agent.speed = prePauseSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EndPoint"))
        {
            GameManager.instance.DamagePlayer(damage);
            this.GetComponent<Collider>().enabled = false;
            WaveSpawner.enemyCount--;
            Destroy(gameObject);
            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentAnim.SetFloat("Speed", agent.speed);

        if (paused)
        {
            prePauseSpeed = agent.speed;
            agent.speed = 0;
            return;
        }

        // Decrease Enemy's HealthBar
        enemyHealthBar.fillAmount = enemyHealth / enemyMaxHealth;
        if(enemyLerpHealth != enemyHealth)
        {
            StartCoroutine (LerpHealth());
        }

        if (cheese)
        {
            if ((enemyHealth / enemyMaxHealth) > 0.75)
            {
;               if (!cheeseMeshes[0].activeInHierarchy)
                {
                    cheeseMeshes[0].SetActive(true);
                    cheeseMeshes[1].SetActive(false);
                    cheeseMeshes[2].SetActive(false);
                    cheeseMeshes[3].SetActive(false);
                    currentAnim = cheeseMeshes[0].GetComponent<Animator>();

                    agent.speed = originalSpeed + (speedIncrease * 0);
                    originalSpeed = agent.speed;
                }
            }
            else if ((enemyHealth / enemyMaxHealth) <= 0.75 && (enemyHealth / enemyMaxHealth) > 0.5)
            {
                if (!cheeseMeshes[1].activeInHierarchy)
                {
                    cheeseMeshes[0].SetActive(false);
                    cheeseMeshes[1].SetActive(true);
                    cheeseMeshes[2].SetActive(false);
                    cheeseMeshes[3].SetActive(false);
                    currentAnim = cheeseMeshes[1].GetComponent<Animator>();

                    agent.speed = originalSpeed + (speedIncrease * 1);
                    originalSpeed = agent.speed;
                }
            }
            else if ((enemyHealth / enemyMaxHealth) <= 0.5 && (enemyHealth / enemyMaxHealth) > 0.25)
            {
                if (!cheeseMeshes[2].activeInHierarchy)
                {
                    cheeseMeshes[0].SetActive(false);
                    cheeseMeshes[1].SetActive(false);
                    cheeseMeshes[2].SetActive(true);
                    cheeseMeshes[3].SetActive(false);
                    currentAnim = cheeseMeshes[2].GetComponent<Animator>();

                    agent.speed = originalSpeed + (speedIncrease * 2);
                    originalSpeed = agent.speed;
                }
            }
            else if ((enemyHealth / enemyMaxHealth) <= 0.25 && (enemyHealth / enemyMaxHealth) > 0)
            {
                if (!cheeseMeshes[3].activeInHierarchy)
                {
                    cheeseMeshes[0].SetActive(false);
                    cheeseMeshes[1].SetActive(false);
                    cheeseMeshes[2].SetActive(false);
                    cheeseMeshes[3].SetActive(true);
                    currentAnim = cheeseMeshes[3].GetComponent<Animator>();

                    agent.speed = originalSpeed + (speedIncrease * 3);
                    originalSpeed = agent.speed;
                }
            }
        }

        if (idle)
        {
            currentAnim.SetBool("Moving", false);
        }
        else
        {
            currentAnim.SetBool("Moving", true);
        }
        if (flying)
        {
            currentAnim.SetBool("Flying", true);
        }

        distanceTravelled += agent.speed * Time.deltaTime;

        if (!slowed)
        {
            RecoverSpeed();
        }
        else
        {
            slowed = false;
        }

        if (burning)
        {

            nextBurn -= Time.deltaTime;
            burnTime -= Time.deltaTime;

            if (burnTime <= 0)
            {
                burning = false;
                burnParticles.Stop();
            }

            if (nextBurn <= 0)
            {
                enemyHealth -= burnDamage;
                nextBurn = burnInterval;
            }

        }
        if(enemyHealth <= 0 && !died)
        {
            Die();
            died = true;
        }
    }

    IEnumerator LerpHealth()
    {

        yield return new WaitForEndOfFrame();
    
        float HealthSmoothing = Mathf.Lerp(enemyLerpHealth, enemyHealth, decreaseRate);
        enemyLerpHealth = HealthSmoothing;
        enemyDamageIndicator.fillAmount = enemyLerpHealth / enemyMaxHealth;
    }

    public void Die()
    {
        pauseScreen.OnEscPressed -= TogglePause;

        Instantiate(cookedSound, transform.position, transform.rotation);
        // particles effects
        GameManager.instance.GetMoney(value);
        GameManager.instance.AddScore(scoreToGive);
        WaveSpawner.enemyCount--;

        Destroy(gameObject);
    }

    public void Slow(float slowFactor)
    {
        if (agent)
        {
            slowParticles.gameObject.SetActive(true);
            agent.speed = originalSpeed * (slowFactor / 100);
            slowed = true;
            slowParticles.Play();
        }
    }

    public void RecoverSpeed()
    {
        if (agent)
        {
            agent.speed = originalSpeed;
            slowParticles.Stop();
        }
    }

    public void Burn(float _burnTime, float _burnRate, float _burnDamage)
    {
        burning = true;
        burnTime = _burnTime;

        burnParticles.gameObject.SetActive(true);
        burnParticles.Play();

        burnInterval = _burnRate;
        nextBurn = burnInterval;
        burnDamage = _burnDamage;
    }
    public void PassAttackingTurret(Turret attackingTurret)
    {
        if(enemyHealth <= 0)
        {
            attackingTurret.IncreaseGaugeKill(getKillFill);
        }
        else
        {
            attackingTurret.IncreaseGaugeHurt(getDamageFill);
        }
    }
}
