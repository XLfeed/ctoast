﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour
{
    public Wave[] wave;
    public Transform spawnPoint;
    public GameObject FlyingSpawner;
    private Transform[] flySpawnPoint;
    public Text waveCountDown;
    public Text currentWaveText;
    private GetAudioSource audioSource;
    private bool played = false;
 
    public float timeBetweenWaves = 5f;
    public float countdown = 5f;

    public static int waveNumber = 0;
    private int phaseNumber;
    private float phaseCountdown;
    public Wave currentWave;
    private bool phasing = false;

    public static int enemyCount = 0;

    public GameObject winScreen;

    private List<int> diffrentEnemyCount;
    private List<GameObject> enemiesToSpawn;

    private bool paused;

    private void Awake()
    {
        PauseScreen pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;

        flySpawnPoint = new Transform[FlyingSpawner.transform.childCount];
        for (int i = 0; i < flySpawnPoint.Length; i++)
        {
            flySpawnPoint[i] = FlyingSpawner.transform.GetChild(i);
        }
    }

    private void Start()
    {
        audioSource = GetComponent<GetAudioSource>();
        diffrentEnemyCount = new List<int>();
        enemiesToSpawn = new List<GameObject>();

        phaseCountdown = 0f;
        currentWave = wave[waveNumber];
        phaseNumber = 0;
        enemyCount = 0;

        currentWaveText.enabled = false;
        waveCountDown.enabled = true;
        Debug.Log("Wave: " + (waveNumber + 1));
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
        Debug.Log("WaveSpawner paused?: " + paused);
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
        {
            return;
        }

        if(enemyCount > 0 && !phasing)
        {
            return;
        }

        if(waveNumber >= wave.Length)
        {
            GameManager.instance.WinGame();
            Debug.Log("Level Completed!!");
            return;
        }

        if (countdown <= 0f)
        {
            played = false;
            currentWave = wave[waveNumber];
            Debug.Log("Wave: " + (waveNumber + 1));
            if (phaseNumber <= currentWave.phases.Length - 1)
            {
                phasing = true;
                countdown = 0f;
                phaseCountdown -= Time.deltaTime;

                if (phaseCountdown <= 0 && phaseNumber < currentWave.phases.Length - 1)
                {
                    StartCoroutine(StartPhase());
                    phaseCountdown = currentWave.phaseRate;
                }
                else if(phaseCountdown <= 0 && phaseNumber == currentWave.phases.Length - 1)
                {
                    StartCoroutine(StartPhase());
                }
            }
            else if(phaseNumber == currentWave.phases.Length)
            {
                waveNumber++;
                countdown = timeBetweenWaves;
                phaseNumber = 0;
            }
        }
        else
        {
            countdown -= Time.deltaTime;
        }
        if(countdown > 0)
        {
            waveCountDown.enabled = true;
            currentWaveText.enabled = false;
            waveCountDown.text = "Next Wave: " + Mathf.Round(countdown) + " s";
        }
        else if(countdown <= 0)
        {
            waveCountDown.enabled = false;
            currentWaveText.enabled = true;
            currentWaveText.text = "WAVE " + (waveNumber + 1);
        }
    }

    IEnumerator StartPhase()
    {
        if(phaseNumber == currentWave.phases.Length - 1)
        {
            phasing = false;
        }

        diffrentEnemyCount.Clear();
        enemiesToSpawn.Clear();

        foreach (EnemyList enemy in currentWave.phases[phaseNumber].enemies)
        {
            diffrentEnemyCount.Add(enemy.count);
            enemiesToSpawn.Add(enemy.enemy);
        }

        for (int i = 0; i < diffrentEnemyCount.Count; i++)
        {
            enemyCount += diffrentEnemyCount[i];
            Debug.Log("Enemy Left: " + enemyCount);
        }

        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            int differentCount = diffrentEnemyCount[i];
            Debug.Log("Number of current enemy type: " + differentCount);

            for (int e = 0; e < differentCount; e++)
            {
                Spawn(enemiesToSpawn[i]);
                Debug.Log("Spawning: " + enemiesToSpawn[i].name);

                yield return new WaitForSeconds(1f / currentWave.phases[phaseNumber].spawnRate);
            }
        }
        phaseNumber++;
    }
        
    void Spawn(GameObject enemyPrefab)
    {
        if (!enemyPrefab.GetComponent<Enemy>().flying)
        {
            if (!played)
            {
                audioSource.PlaySound(0);
                played = true;
            }
            Instantiate(enemyPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
        }
        else
        {
            if (!played)
            {
                audioSource.PlaySound(1);
                played = true;
            }
            int randX = Random.Range(0, flySpawnPoint.Length - 1);
            Instantiate(enemyPrefab, flySpawnPoint[randX].transform.position, flySpawnPoint[randX].transform.rotation);
        }
    }
}
