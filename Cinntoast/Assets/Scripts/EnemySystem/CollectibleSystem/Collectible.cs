﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public int value = 10;
    public float lifeSpan = 5;

    private Transform parent = null;
    private SpawnPointsArray spawner;

    private void Start()
    {
        spawner = FindObjectOfType<SpawnPointsArray>();
    }

    void OnMouseDown()
    {
        if(parent != null)
        {
            GameManager.instance.GetBling();

            GameManager.instance.GetMoney(value);
            DestroyThis();
        }
    }

    private void Update()
    {
        lifeSpan -= Time.deltaTime;
        if(lifeSpan <= 0)
        {
            DestroyThis();
        }
    }

    public void GetSpawnPoint(Transform spawnPoint)
    {
        parent = spawnPoint;
    }

    void DestroyThis()
    {
        spawner.FreeSpawnPoint(parent);
        Destroy(gameObject);
    }
}
