﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointsArray : MonoBehaviour
{
    public float spawnInterval = 5f;
    private float spawnTimer;

    public GameObject collectible;

    private List<Transform> spawnPoints;

    private int spawnerRange;
    private bool spawned;

    private int x;

    private int counter;

    private PauseScreen pauseScreen;
    private bool paused = false;

    private void Awake()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
        spawnTimer = spawnInterval;

        spawnPoints = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            spawnPoints.Add(transform.GetChild(i));
        }

        spawnerRange = spawnPoints.Count - 1;
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
        {
            return;
        }

        //Timer
        spawnTimer -= Time.deltaTime;

        Debug.Log("Time To Next Spawn: " + spawnTimer + ". Counter: " + counter);

        if (spawnTimer <= 0.0f)
        {
            float interval = spawnInterval / (GameManager.instance.turretCount + 1);
            spawnTimer = interval;
            spawned = false;

            //spawner logic
            x++;
            if(x > spawnerRange)
            {
                x = 0;
            }

            int y = Random.Range(0, spawnerRange);

            Debug.Log("x = " + x + ". y = " + y);

            //Spawn collectibles
            if (x == y && !spawned)
            {
                SpawnCollectible(x);
            }
            spawned = true;
            counter++;
        }
    }

    void SpawnCollectible(int x)
    {
        GameObject _collectible = Instantiate(collectible, spawnPoints[x].position, Quaternion.identity);
        _collectible.GetComponent<Collectible>().GetSpawnPoint(spawnPoints[x]);
        spawnPoints.RemoveAt(x);
        spawnerRange--;
        spawned = true;
    }

    public void FreeSpawnPoint(Transform spawnPointX)
    {
        spawnPoints.Add(spawnPointX);
        spawnerRange++;
    }
}
