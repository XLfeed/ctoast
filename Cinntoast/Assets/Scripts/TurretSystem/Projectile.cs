﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Transform target;
    private Enemy targetScript;
    private Rigidbody rigidbody;

    public float speed;
    public float lifeSpan;

    private float damage;
    private float explosionRadius;
    private Vector3 dir;
    private float distanceLeft;
    private bool particlesSpawned = false;

    private Turret SpawnTurret;

    public GameObject bulletImpactParticle;

    public GetAudioSource gettingAS;

    private MeshRenderer meshRend;
    private Collider myCollider;
    public GameObject oilProjectile;

    private bool paused;
    private PauseScreen pauseScreen;

    public void Awake()
    {
        meshRend = GetComponent<MeshRenderer>();
        myCollider = GetComponent<Collider>();

        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;
    }

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
        if (!paused)
        {
            rigidbody.velocity = dir.normalized * speed * Time.fixedDeltaTime;
        }
    }

    public void RocketTarget(Transform theTarget, Enemy targetEnemy, float explosionDamage, float radius, Turret parentTurret)
    {
        target = theTarget;
        targetScript = targetEnemy;
        damage = explosionDamage;
        explosionRadius = radius;
        SpawnTurret = parentTurret;
        dir = target.position - transform.position;
    }
    public void BulletTarget(Transform theTarget, Enemy targetEnemy, float bulletDamage, Turret parentTurret)
    {
        target = theTarget;
        targetScript = targetEnemy;
        damage = bulletDamage;
        SpawnTurret = parentTurret;

        dir = target.position - transform.position;
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Enemy")
        {
            if(explosionRadius > 0)
            {
                if (bulletImpactParticle)
                {
                    GameObject particles = Instantiate(bulletImpactParticle, transform.position, transform.rotation);
                    Destroy(particles, 1.0f);
                }

                gettingAS.PlaySound(0);
                Explode();

                DestroySelf();
            }
            else if(explosionRadius <= 0)
            {
                if (bulletImpactParticle)
                {
                    GameObject particles = Instantiate(bulletImpactParticle, transform.position, transform.rotation);
                    Destroy(particles, 1.0f);
                }

                gettingAS.PlaySound(0);
                targetScript = col.GetComponent<Enemy>();
                Damage(targetScript);
                col.GetComponent<Enemy>().PassAttackingTurret(SpawnTurret);
                DestroySelf();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
        {
            rigidbody.velocity = Vector3.zero;
            return;
        }

        transform.LookAt(target);
        lifeSpan -= Time.deltaTime;
        if (lifeSpan <= 0)
        {
            if (explosionRadius > 0 && !particlesSpawned)
            {
                particlesSpawned = true;
                GameObject particles = Instantiate(bulletImpactParticle, transform.position, transform.rotation);
                Destroy(particles, 1.0f);

                Explode();

                DestroySelf();
                return;
                //as the rocket approaches the target point, it explodes and damages surrounding enemies
            }
            else if (explosionRadius <= 0)
            {
                DestroySelf();
                return;
            }
        }
    }
    private void FixedUpdate()
    {
        rigidbody.velocity = dir.normalized * speed * Time.fixedDeltaTime;
    }
    void DestroySelf()
    {
        pauseScreen.OnEscPressed -= TogglePause;

        myCollider.enabled = false;

        if(explosionRadius > 0)
        {
            oilProjectile.SetActive(false);
        }
        if (explosionRadius <= 0)
        {
            meshRend.enabled = false;
        }
        Destroy(gameObject);
    }
    void Damage(Enemy theTarget)
    {
        //do damage
        theTarget.GetDamage(damage);
    }
    void Explode() //Damage surrounding enemies when it explode
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                Damage(collider.GetComponent<Enemy>());
                collider.GetComponent<Enemy>().PassAttackingTurret(SpawnTurret);
            }
        }
    }
}
