﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Towermerger : MonoBehaviour
{
    //[HideInInspector]
    public Node firstSelect;
    //[HideInInspector]
    public Node secondSelect;

    public static bool dragging; //checks whether player is holding mouse 1 and moving mouse

    private bool tilecheck = false; //sends yes or no on whether cursor is hitting a tile that has a tower that can be merged

    private TurretList turretselected;

    public bool mergecheckcheck;

    private void Update()
    {
        if (firstSelect && secondSelect)
        {
            mergecheckcheck = MergeCheck();
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 100);
        Collider currentHoveringCollider = hit.collider;
        if (currentHoveringCollider && currentHoveringCollider.CompareTag("Tile"))
        {
            tilecheck = true;
        }
        else
        {
            tilecheck = false;
        }
    }
    public void Passingtile(Node holdingtile)
    {
        firstSelect = holdingtile; //Node script from specific tile is now stored as a variable in this script
        firstSelect.mergeMarking = 1;
    }
    public bool MergeCheck()
    {
        if (secondSelect != null && firstSelect != null && secondSelect.currentTurret != null)
        {
            float distance = Vector3.Distance(firstSelect.transform.position, secondSelect.transform.position);
            if (distance <= firstSelect.mergeRange)
            {
                if (secondSelect.mergeMarking == 0 && secondSelect.currentTier < secondSelect.currentTurret.maxTier)
                {
                    bool isSameTower = SameTowerCheck();
                    if (isSameTower)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public void ResetScript()
    {
        if (firstSelect)
        {
            firstSelect.mergeMarking = 0;
        }
        firstSelect = null;
        secondSelect = null;
        dragging = false;
    }
    public void SetSecondTile(Node secondTile)
    {
        secondSelect = secondTile; 
    }
    public void RemoveSecondTile()
    {
        secondSelect = null;
    }

    private bool SameTowerCheck()
    {
        if(firstSelect.currentTurret.CanBeUpgraded && secondSelect.currentTurret.CanBeUpgraded 
            && firstSelect.currentTurret.towerIndex == secondSelect.currentTurret.towerIndex)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
