﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSelection : MonoBehaviour
{
    public Node currentSelect;

    public void CurrentSelection(Node currentTile)
    {
        if(currentSelect != null)
        {
            currentSelect.selected = false;
        }

        currentSelect = currentTile;
        currentSelect.selected = true;
    }

    public void RemoveSelection()
    {
        if(currentSelect != null)
        {
            currentSelect.selected = false;
        }
        currentSelect = null;
    }
}
