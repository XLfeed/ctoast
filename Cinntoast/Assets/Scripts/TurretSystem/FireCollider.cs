﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCollider : MonoBehaviour
{
    public Turret thisTurret;

    public Vector3 halfBoxLength;
    public Vector3 CentreOffSet;

    public bool firing;

    private void Awake()
    {
        transform.localPosition = CentreOffSet;
    }

    private void Update()
    {
        if (firing)
        {
            Collider[] colliders = Physics.OverlapBox(transform.position, halfBoxLength);

            foreach (Collider collider in colliders)
            {
                if (collider.CompareTag("Enemy"))
                {
                    thisTurret.PassEnemy(collider);
                }
            }
            firing = false;
        }
    }

    public void Fire()
    {
        firing = true;
    }
}
