﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret : MonoBehaviour
{
    #region Variables
    [Header("Unity Required Fields")]
    public Transform rotatePoint;
    private Transform target;
    public Enemy targetEnemy;
    public GameObject gaugeBarFull;
    public GameObject gaugeBar;
    public GetAudioSource gettingAS;
    private PauseScreen pauseScreen;
    private bool paused = false;

    public GameObject projectilePrefab;
    public Transform firePoint;

    [Header("Basic Attributes")]
    public float range = 15f;
    public float rotateSpeed = 10f;
    public float fireRate = 1f;
    public float bulletDamage;
    public float mergeRange;

    [Header("Rocket Turret")]
    public float explosionDamage;
    public float explosionRadius;

    [Header("Lazer Turret")]
    public bool useLazer = false;
    public float damageOverTime;
    public float slowFactor;
    public LineRenderer beam;
    public ParticleSystem lazerPart01;
    public ParticleSystem lazerPart02;
    public GameObject LazerFlash;
    

    [Header("Area Turret")]
    public float areaDamage;
    public float burnDamage;
    public float burnDuration;
    public float burnRate;
    public GameObject torchMouth;
    public ParticleSystem fireFX;

    private float nextFire = 0f;
    [SerializeField]
    private int targetIndex; 

    [Header("Performence Settings")]
    [Range(20f, 60f)]
    public float targetUpdateRate = 20f;


    [Header("Level Up")]
    public float gaugeMaxFill;
    public float currentFill;
    public Image UpgradeBar;
    public bool CanBeUpgraded = false;
    public bool killedsomething;
    public int towerIndex;
    public int maxTier = 2;
    private bool playedOnce = false;

    #endregion

    private void Awake()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;
    }

    // Start is called before the first frame update
    void Start()
    {
        gaugeBar.SetActive(true);
        gaugeBarFull.SetActive(false);

        InvokeRepeating("UpdateTarget", 0f, 1/targetUpdateRate); //control the number of times at which UpdateTarget method is called per second

        UpgradeBar.fillAmount = currentFill / gaugeMaxFill;

        currentFill = 0f;
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
    }

    public void Unsubscribe()
    {
        pauseScreen.OnEscPressed -= TogglePause;
    }

    //called from corresponded 'Node' Script, Set 'targetIndex'
    public void SetTarget(int index)
    {
        targetIndex = index;
    } 

    void UpdateTarget()
    {
        //search for nearest target
        if (targetIndex == 0)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            float shortestDistance = Mathf.Infinity; //Set default value
            GameObject nearestEnemy = null;

            foreach (GameObject enemy in enemies)
            {
                if (enemy.activeInHierarchy)
                {

                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

                    if (distanceToEnemy < shortestDistance)
                    {
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = enemy;
                    }

                    if (nearestEnemy != null && shortestDistance <= range)
                    {

                        target = nearestEnemy.transform;
                        targetEnemy = target.GetComponent<Enemy>();
                    }
                    else
                    {
                        target = null;
                    }
                }
                else
                {
                    target = null;
                }
            }
        }
        //search for first target
        else if (targetIndex == 1)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            float mostDistanceTravelled = 0;
            GameObject firstEnemy = null;

            foreach (GameObject enemy in enemies)
            {
                if (enemy.activeInHierarchy)
                {
                    Enemy enemyScript = enemy.GetComponent<Enemy>();
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

                    if (distanceToEnemy <= range && enemyScript.distanceTravelled > mostDistanceTravelled)
                    {
                        mostDistanceTravelled = enemyScript.distanceTravelled;
                        firstEnemy = enemy;
                    }
                    if (firstEnemy != null)
                    {
                        target = firstEnemy.transform;
                        targetEnemy = target.GetComponent<Enemy>();
                    }
                    else
                    {
                        target = null;
                        Debug.Log("No enemy in range!");
                    }
                }
                else
                {
                    target = null;
                }
            }
        }
        //search for last enemy
        else if (targetIndex == 2)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            float leastDistanceTravelled = Mathf.Infinity;
            GameObject lastEnemy = null;

            foreach (GameObject enemy in enemies)
            {
                if (enemy.activeInHierarchy)
                {
                    Enemy enemyScript = enemy.GetComponent<Enemy>();
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

                    if (distanceToEnemy <= range && enemyScript.distanceTravelled < leastDistanceTravelled)
                    {
                        leastDistanceTravelled = enemyScript.distanceTravelled;
                        lastEnemy = enemy;
                    }
                    if (lastEnemy != null)
                    {
                        target = lastEnemy.transform;
                        targetEnemy = target.GetComponent<Enemy>();
                    }
                    else
                    {
                        target = null;
                        Debug.Log("No enemy in range!");
                    }
                }
                else
                {
                    target = null;
                }
            }
        }
        //search for strongest enemy
        else if (targetIndex == 3)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            float greatestMaxHealth = 0f;
            GameObject strongestEnemy = null;

            foreach (GameObject enemy in enemies)
            {
                if (enemy.activeInHierarchy)
                {
                    Enemy enemyScript = enemy.GetComponent<Enemy>();
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

                    if (distanceToEnemy <= range && enemyScript.enemyMaxHealth > greatestMaxHealth)
                    {
                        greatestMaxHealth = enemyScript.enemyMaxHealth;
                        strongestEnemy = enemy;
                    }
                    if (strongestEnemy != null)
                    {
                        target = strongestEnemy.transform;
                        targetEnemy = target.GetComponent<Enemy>();
                    }
                    else
                    {
                        target = null;
                        Debug.Log("No enemy in range!");
                    }
                }
                else
                {
                    target = null;
                }
            }
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
        {
            return;
        }
        //when no enemy is in range
        if (target == null)
        {
            if(areaDamage > 0)
            {
                gettingAS.StopSound(0);
                playedOnce = false;
                fireFX.gameObject.SetActive(true);
                fireFX.Stop();
            }

            if(useLazer && beam.enabled) //stop lazer from attacking when no enemy in range
            {
                gettingAS.StopSound(0);
                playedOnce = false;
                beam.enabled = false;
                lazerPart01.Stop();
                lazerPart02.Stop();
            }
            return;
        }

        //turn the turret to look at the target enemy
        LockOnTarget();

        //for Laser tower
        if (useLazer) //manually check this boolean in inspector for Lazer Turret, uncheck for other turrets
        {
            Lazer();
        }
        //for AOE tower
        else if (areaDamage > 0)
        {
            EmitDamage();
        }
        //for Rocket Turret, shoots projectile that explode
        else if (explosionRadius > 0)
        {
            if (nextFire <= 0f)
            {
                FireRocket();

                nextFire = 1 / fireRate;
            }
            nextFire -= Time.deltaTime;
        }
        //for Normal tower
        else
        {
            if (nextFire <= 0f)
            {
                Fire();

                nextFire = 1 / fireRate;
            }
            nextFire -= Time.deltaTime;
        }
        UpgradeBar.fillAmount = currentFill / gaugeMaxFill;
    }
    //check if tower can be upgraded
    public void AllowUpgrade()
    {
        gaugeBar.SetActive(false);
        gaugeBarFull.SetActive(true);
        CanBeUpgraded = true;
    }
    //show that the tower is at max tier
    public void MaxTierReached()
    {
        CanBeUpgraded = false;
        /*gaugeBar.SetActive(false);
        gaugeBarMax.SetActive(true);*/
    }

    //rotate the tower to face the enemy
    public void LockOnTarget()
    {
        Vector3 dir = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotatePoint.rotation, lookRotation, Time.deltaTime * rotateSpeed).eulerAngles;
        // convert Quaternion to eulerangle and Smoothen the rotation

        rotatePoint.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    //use lazer
    void Lazer()
    {
        if (!beam.enabled)
        {
            beam.enabled = true;
            lazerPart01.Play();
            lazerPart02.Play();
        }
        if (!playedOnce)
        {
            gettingAS.PlaySound(0);
            playedOnce = true;
        }

        beam.SetPosition(0, firePoint.position);
        beam.SetPosition(1, target.position);

        Vector3 dir = firePoint.position - target.position;
        lazerPart01.transform.rotation = Quaternion.LookRotation(dir);
        lazerPart02.transform.rotation = Quaternion.LookRotation(dir);

        Instantiate(LazerFlash, firePoint.transform.position, firePoint.transform.rotation);

        lazerPart01.transform.position = target.position + dir.normalized * -0.1f;
        lazerPart02.transform.position = target.position + dir.normalized * -0.1f;

        targetEnemy.GetDamage((damageOverTime) * Time.deltaTime);
        targetEnemy.Slow(slowFactor);
        targetEnemy.PassAttackingTurret(this);
    }
    //shoot normal projectiles
    void Fire()
    {
        GameObject bullet = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);

        Projectile projectileScript = bullet.GetComponent<Projectile>();
        gettingAS.PlaySound(0);

        if(projectileScript != null)
        {
            projectileScript.BulletTarget(target, targetEnemy, bulletDamage, this);

        }
    }
    //shoot explosive projectiles
    void FireRocket()
    {
        GameObject rocket = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        Projectile projectileScript = rocket.GetComponent<Projectile>();
        gettingAS.PlaySound(0);

        if (projectileScript != null)
        {
            projectileScript.RocketTarget(target, targetEnemy, explosionDamage, explosionRadius, this);
        }
    }
    //does AOE damage to surrounding
    void EmitDamage()
    {
        if (!playedOnce)
        {
            gettingAS.PlaySound(0);
            playedOnce = true;
        }
        torchMouth.GetComponent<FireCollider>().Fire();
        fireFX.gameObject.SetActive(true);
        fireFX.Play();
    }

    public void PassEnemy(Collider collider)
    {
        Enemy enemy = collider.GetComponent<Enemy>();
        enemy.GetDamage(areaDamage * Time.deltaTime);
        enemy.Burn(burnDuration, burnRate, burnDamage);
        targetEnemy.PassAttackingTurret(this);
        Debug.Log("Enemy Blowtorched");
    }

    //change in stats after upgrade
    public void AddOnStats(AddOnTurretStats addOn)
    {
        range += addOn.range;
        fireRate += addOn.fireRate;
        bulletDamage += addOn.bulletDamage;

        explosionDamage += addOn.explosionDamage;
        explosionRadius += addOn.explosionRadius;

        damageOverTime += addOn.damageOverTime;
        slowFactor += addOn.slowFactor;

        areaDamage += addOn.areaDamage;
        burnDamage += addOn.burnDamage;
        burnDuration += addOn.burnDuration;
        burnRate += addOn.burnRate;
    }

    //increase gauge fill when tower kills an enemy
    public void IncreaseGaugeKill(float gaugeFill)
    {
        if(currentFill < gaugeMaxFill)
        {
            currentFill += gaugeFill;
        }
        else
        {
            CanBeUpgraded = true;
            return;
        }
    }
    //increase gause fill when tower hurts an enemy
    public void IncreaseGaugeHurt(float gaugeFill)
    {
        if ((currentFill < gaugeMaxFill && useLazer) || (currentFill < gaugeMaxFill && areaDamage > 0))
        {
            currentFill += gaugeFill * Time.deltaTime;
        }
        else if (currentFill < gaugeMaxFill && !useLazer)
        {
            currentFill += gaugeFill;
        }
        else
        {
            CanBeUpgraded = true;
            return;
        }
      
    } 

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    } //only show in scene
}
