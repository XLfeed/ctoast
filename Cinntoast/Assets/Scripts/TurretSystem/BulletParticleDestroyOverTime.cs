﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletParticleDestroyOverTime : MonoBehaviour
{
    float bulletParticleTime = 3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bulletParticleTime = bulletParticleTime - Time.deltaTime;
        if (bulletParticleTime < 0)
        {
            Destroy(gameObject);
        }
    }
}
