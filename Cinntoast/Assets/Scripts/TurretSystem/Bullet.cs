﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;
    private Enemy targetScript;

    public GameObject hitEffect;
    public float speed;
    public float lifeSpan;

    private float damage;

    public void SetTarget(Transform theTarget, Enemy targetEnemy, float bulletDamage) //Set the bullet's target as the target of the turret
    {
        target = theTarget; //transform of the targeted enemy
        targetScript = targetEnemy; //<Enemy> script attached to the target enemy
        damage = bulletDamage; //damage of the bullet that the turret fires
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null) //if the target enemy is destroyed before the bullet reaches it, this bullet will destroy itself
        {
            DestroySelf();
            return;
        }

        lifeSpan -= Time.deltaTime; //upon being spawned, life span of the bullet starts counting down, when life span reaches 0, bullet will despawn
        if(lifeSpan <= 0)
        {

             DestroySelf();

        }

        Vector3 dir = target.position - transform.position; 
        float distanceAFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distanceAFrame)
        {
            GameObject hitEffectInst = Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(hitEffectInst, 1.0f);

            Damage(targetScript);

            DestroySelf();
            return;
        }

        transform.Translate(dir.normalized * distanceAFrame, Space.World);
        transform.LookAt(target);
        
        //bullet will calculate the direction at which it has to travel to reach the target enemy every frame
        //when the distance between bullet and the target is less than or equals to the distance that the bullet travels per frame
        //it will be taken as the bullet has hit the target and damage it

    }
    void DestroySelf()
    {
        Destroy(gameObject);
    }
    void Damage(Enemy theTarget)
    {
        //do damage
        theTarget.GetDamage(damage);
    }
}
