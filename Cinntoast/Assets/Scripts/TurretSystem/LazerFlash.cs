﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerFlash : MonoBehaviour
{
    float lazerFlashTime = 0.15f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        lazerFlashTime = lazerFlashTime - Time.deltaTime;
        if (lazerFlashTime < 0)
        {
            Destroy(gameObject);
        }
    }
}
