﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableGrid : MonoBehaviour
{
    private void LateUpdate()
    {
        if (GetComponent<Node>().correspondingGrid)
        {
            GetComponent<Node>().correspondingGrid.Block();
        }

    }
}
