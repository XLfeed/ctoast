﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour
{
    public bool isOccupied;
    public int visited = -1;
    public int x;
    public int y;

    public void Clear()
    {
        isOccupied = false;
    }
    public void Block()
    {
        isOccupied = true;
    }
}
