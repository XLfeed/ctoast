﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCoordinates : MonoBehaviour
{
    public int rows;
    public int columns;

    public Transform[] tiles;

    private PathChecker pathChecker;

    // Start is called before the first frame update
    void Awake()
    {
        tiles = new Transform[rows * columns];
        StartCoroutine(SetCoordinates());
        pathChecker = FindObjectOfType<PathChecker>();
    }
    IEnumerator SetCoordinates()
    {
        yield return new WaitForSeconds(0.04f);
        int x = 0;
        int y = 0;

        for (int i = 0; i < tiles.Length; i++)
        {
            transform.GetChild(i).GetComponent<Node>().x = x;
            transform.GetChild(i).GetComponent<Node>().y = y;

            transform.GetChild(i).GetComponent<Node>().correspondingGrid = pathChecker.transform.GetChild(i).GetComponent<GridController>();

            if (y < rows - 1)
            {
                y++;
            }
            else if(y >= rows - 1)
            {
                y = 0;
                x++;
            }
        }
    }
}
