﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathChecker : MonoBehaviour
{
    public List<GameObject> path = new List<GameObject>();
    public bool findDistance = false;
    public int rows = 12;
    public int columns = 12;
    public int scale = 1;
    public GameObject gridPrefab;
    private Vector3 startingPoint;
    public GameObject[,] gridArray;
    public int startX = 0;
    public int startY = 0;
    public int endX = 10;
    public int endY = 10;

    public int posiPath = 0;

    //[Header("Optimization Setting")]
    //[Range(1, 100)]
    //public float repeatRate = 30f;

    // Update is called once per frame
    void Awake()
    {
        startingPoint = transform.position;
        gridArray = new GameObject[columns, rows];
        if (gridPrefab)
        {
            GenerateGrid();
        }
        else
        {
            Debug.Log("Missing GridPrefab!!");
        }
    }

    private void Update()
    {
        if (findDistance)
        {
            SetDistance();
            SetPath();
        }
        findDistance = false;
    }
    public void FindDistance()
    {
        posiPath = 0;
        findDistance = true;
    }

    void GenerateGrid()
    {
        for(int i = 0; i < columns; i++)
        {
            for (int q = 0; q < rows; q++)
            {
                GameObject gridN = Instantiate(gridPrefab, new Vector3(startingPoint.x + scale * i, startingPoint.y, startingPoint.z + scale * q), Quaternion.identity);
                gridN.transform.SetParent(gameObject.transform);
                gridN.GetComponent<GridController>().x = i;
                gridN.GetComponent<GridController>().y = q;
                gridArray[i, q] = gridN;
            }
        }
    }

    void SetDistance()
    {
        InitialSetUp();
        for(int step = 1; step < rows * columns; step++)
        {
            foreach(GameObject gridN in gridArray)
            {
                if(gridN.GetComponent<GridController>().visited == step - 1)
                {
                    TestFourDirection(gridN.GetComponent<GridController>().x, gridN.GetComponent<GridController>().y, step);
                }
            }
        }

    }

    void SetPath()
    {
        int step;
        int x = endX;
        int y = endY;
        List<GameObject> tempList = new List<GameObject>();
        path.Clear();
        if(gridArray[endX, endY] && gridArray[endX, endY].GetComponent<GridController>().visited > 0)
        {
            path.Add(gridArray[x, y]);
            step = gridArray[x, y].GetComponent<GridController>().visited - 1;
            posiPath = 1;
        }
        else
        {
            posiPath = -1;
            Debug.Log("End Point can't be reached");
            return;
        }
        for (int i = step; step > -1; step--)
        {
            if(TestDirection(x, y, step, 1))
            {
                tempList.Add(gridArray[x, y + 1]);
            }
            if (TestDirection(x, y, step, 2))
            {
                tempList.Add(gridArray[x + 1, y]);
            }
            if (TestDirection(x, y, step, 3))
            {
                tempList.Add(gridArray[x, y - 1]);
            }
            if (TestDirection(x, y, step, 4))
            {
                tempList.Add(gridArray[x - 1, y]);
            }
            GameObject tempGrid = FindClosest(gridArray[endX, endY].transform, tempList);
            path.Add(tempGrid);
            x = tempGrid.GetComponent<GridController>().x;
            y = tempGrid.GetComponent<GridController>().y;
            tempList.Clear();
        }

    }

    void InitialSetUp()
    {
        foreach (GameObject gridN in gridArray)
        {
            gridN.GetComponent<GridController>().visited = -1;
        }

        gridArray[startX, startY].GetComponent<GridController>().visited = 0;
    }

    bool TestDirection(int x, int y, int step, int direction)
    {
        //int direction is the direction to go next, 1 is up, 2 is right, 3 is down, 4 is left

        switch (direction)
        {
            case 1:
                if (y + 1 < rows && gridArray[x, y + 1] && gridArray[x, y + 1].GetComponent<GridController>().visited == step)
                    return true;
                else
                    return false;
            case 2:
                if (x + 1 < columns && gridArray[x + 1, y] && gridArray[x + 1, y].GetComponent<GridController>().visited == step)
                    return true;
                else
                    return false;
            case 3:
                if (y - 1 > -1 && gridArray[x, y - 1] && gridArray[x, y - 1].GetComponent<GridController>().visited == step)
                    return true;
                else
                    return false;
            case 4:
                if (x - 1 > -1 && gridArray[x - 1, y] && gridArray[x - 1, y].GetComponent<GridController>().visited == step)
                    return true;
                else
                    return false;
        }
        return false;
    }

    void TestFourDirection(int x, int y, int step)
    {
        if(TestDirection(x, y, -1, 1))
        {
            SetVisited(x, y + 1, step);
        }
        if (TestDirection(x, y, -1, 2))
        {
            SetVisited(x + 1, y, step);
        }
        if (TestDirection(x, y, -1, 3))
        {
            SetVisited(x, y - 1, step);
        }
        if (TestDirection(x, y, -1, 4))
        {
            SetVisited(x - 1, y, step);
        }
    }

    void SetVisited (int x, int y, int step)
    {
        if(gridArray[x, y] && !gridArray[x, y].GetComponent<GridController>().isOccupied)
        {
            gridArray[x, y].GetComponent<GridController>().visited = step;
        }
    }
    GameObject FindClosest(Transform targetLocation, List<GameObject> list)
    {
        float curretDistance = scale * rows * columns;
        int indexNumber = 0;
        for (int i = 0; i < list.Count; i++)
        {
            if(Vector3.Distance(targetLocation.position, list[i].transform.position) < curretDistance)
            {
                curretDistance = Vector3.Distance(targetLocation.position, list[i].transform.position);
                indexNumber = i;
            }
        }
        return list[indexNumber];
    }
    public int Obstruct()
    {
        return posiPath;
    }
}
