﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBallController : MonoBehaviour
{
    private PathChecker thePath;

    public float startSpeed = 10f;
    private float currentSpeed;

    public float pointOffset = 0.2f;

    private GameObject target;
    public int waypointIndex = 0;
    public float distanceToNextWaypoint;

    // Start is called before the first frame update
    void Start()
    {
        thePath = FindObjectOfType<PathChecker>();
        currentSpeed = startSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if(waypointIndex > 1)
        {
            distanceToNextWaypoint = Vector3.Distance(transform.position, target.transform.position);
            Vector3 dir = target.transform.position - transform.position;
            dir.y = 0;
            transform.Translate(dir.normalized * currentSpeed * Time.deltaTime, Space.World);

            if (distanceToNextWaypoint <= pointOffset)
            {
                GetNextWaypoint();
            }
        }
        else
        {
            waypointIndex = thePath.path.Count - 2;
            target = thePath.path[waypointIndex];
        }
    }

    void GetNextWaypoint()
    {
            waypointIndex--;
            target = thePath.path[waypointIndex];
    }
}
