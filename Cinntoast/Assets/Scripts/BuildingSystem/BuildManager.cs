﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance; //for creating a singleton of this script, so other scripts can access this script without referencing

    public TurretList[] turretList; //where all the turrets will be stored in, edit in Inspector

    private Node selectedNode; //store the tile that is currently selected



    private void Awake()
    {
        if(instance != null) //make sure there is only one instance of this script to prevent errors
        {

            Debug.LogError("More than 1 Instance of BuildManager");
            return;
        }

        instance = this;
    }
    [HideInInspector]
    public TurretList turretToBuild = null; //to store the one <TurretList> that will be selected when the player select a turret to build

    public TurretList GetTurretToBuild(int turretIndex) //trigger by <Shop> script when a turret that player wants to build is selected
    {
        if(turretIndex <= turretList.Length - 1) //check the number of turrets stored in turretList
        {
            turretToBuild = turretList[turretIndex]; //store the selected <TurretList> in the turretList array in turretToBuild variable
            return turretToBuild;
        }
        else
        {
            Debug.Log("No Turret assigned to this button");
            return null;
        }

    }
    public TurretList PassDownTurretToBuild() //called from <Node> script when a tile is selected
    {
        if(turretToBuild != null) //there is a <TurretList> selected
        {
            return turretToBuild;
        }
        else
        {
            return null;
        }
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(1)) // right click resets turretToBuild
        {
            turretToBuild = null;
        }
    }
    public void Deselect() //resets turretToBuild when other tiles are selected
    {
        turretToBuild = null;
    }
}
