﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class Node : MonoBehaviour
{
    #region Variables
    public GameObject mergeRangeIndicator;
    public GameObject targetRangeIndicator;
    public GameObject mouseOverIndicator;
    public GameObject denyIndicator;
    public bool selected = false;

    public Color hoverColor;
    public Color mergingColor;
    public Color denyColor;
    public Color preparingColor;
    private Color origColor;

    private PauseScreen pauseScreen;
    private bool paused = false;

    public TurretList turret;
    
    public Turret currentTurret; //variable for storing the <Turret> script in the <TurretList> script passed in

    private Shop shop;

    public GetAudioSource gettingAS;

    public float mergeRange = 15f;

    #region Build System Variable
    [HideInInspector]
    public bool path1Max = false;
    [HideInInspector]
    public bool path2Max = false;
    [HideInInspector]
    public bool path3Max = false;
    [HideInInspector]
    public int path1UpgradeStatus;
    [HideInInspector]
    public int path2UpgradeStatus;
    [HideInInspector]
    public int path3UpgradeStatus;
    [HideInInspector]
    public int targetSettingIndex;
    [HideInInspector]
    public int canBuild = 0;
    [HideInInspector]
    public int currentTier = 0;
    #endregion

    private AddOnTurretStats addOn;//the total increace in stats after upgrading stored in this script
    [HideInInspector]
    public AddOnTurretStats newAddOn; //the new total increace in stats after upgrading to check any changes in values

    public int x, y;//coordinates of the tile
    public GridController correspondingGrid;

    private PathChecker pathChecker;

    private bool isNotNeeded;
    private bool isEndTile;
    private bool isSpawnTile;

    public Towermerger towerMerging;
    private TowerSelection towerSelection;
    public int mergeMarking = 0;
    public bool upgrading = false;

    #endregion

    private void Awake()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;

        if (gameObject.tag == "OutOfBound")
        {
            isNotNeeded = true;
        }
        else
        {
            isNotNeeded = false;
        }
        if (gameObject.tag == "EndTile")
        {
            isEndTile = true;
        }
        else
        {
            isEndTile = false;
        }
        if (gameObject.tag == "SpawnTile")
        {
            isSpawnTile = true;
        }
        else
        {
            isSpawnTile = false;
        }
        if (gameObject.CompareTag("Tile"))
        {
            mergeRangeIndicator.SetActive(false);
            targetRangeIndicator.SetActive(false);
        }
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
    }

    // Start is called before the first frame update
    void Start()
    {
        towerSelection = FindObjectOfType<TowerSelection>();

        if (gameObject.CompareTag("Tile"))
        {
            denyIndicator.SetActive(false);
            targetRangeIndicator.SetActive(false);
        }

        shop = FindObjectOfType<Shop>();

        path1UpgradeStatus = 0;
        path2UpgradeStatus = 0;
        path3UpgradeStatus = 0;

        pathChecker = FindObjectOfType<PathChecker>();
        towerMerging = FindObjectOfType<Towermerger>();

        turret = null;

        addOn = newAddOn;
    }

    private void OnMouseEnter()
    {
        if (isNotNeeded)
        {
            canBuild = 0;
            Debug.Log("Out Of Bound!!");
            return;
        }
        if (EventSystem.current.IsPointerOverGameObject())
        {
            canBuild = 0;
            return;
        }
        //check for whether any eventsystem (UI elements) is in the way, if yes, do not change the colour of the tile
        //when there is eventsystem in the scene, it will return an error, so only un-comment it when eventsystem is added
        if (isEndTile)
        {
            canBuild = 0;
            Debug.Log("Cannot build on End Tile!");
            return;
        }
        if (isSpawnTile)
        {
            canBuild = 0;
            Debug.Log("Cannot build on Start Tile!");
            return;
        }
        if (!Towermerger.dragging)
        {
            StartCoroutine(ChangeTileColor());
        }
        else if (Towermerger.dragging)
        {
            if(mergeMarking == 0)
            {
                towerMerging.SetSecondTile(this);

                //check whether mouse is hovering over another tile with tower
                bool canMerge = towerMerging.MergeCheck();
                Debug.Log(towerMerging.MergeCheck());
                if (canMerge)
                {
                    canBuild = 1;
                }
                else
                {
                    canBuild = -1;
                }
            }
            else if (mergeMarking == 1)
            {
                canBuild = -1;
            }

            if (canBuild == 1 || canBuild == 0)
            {
                mouseOverIndicator.SetActive(true);
                denyIndicator.SetActive(false);
            }
            else if (canBuild == -1)
            {
                denyIndicator.SetActive(true);
                mouseOverIndicator.SetActive(false);
            }
        }
        //change the colour of the tile accordingly
    }

    IEnumerator ChangeTileColor()
    {
        if (correspondingGrid)
        {
            correspondingGrid.Block();
        }

        pathChecker.FindDistance();
        yield return new WaitForEndOfFrame();
        canBuild = pathChecker.Obstruct();

        if (canBuild == 1)
        {
            mouseOverIndicator.SetActive(true);
            denyIndicator.SetActive(false);
        }
        else if (canBuild == -1)
        {
            denyIndicator.SetActive(true);
            mouseOverIndicator.SetActive(false);
        }
    }

    private void OnMouseExit()
    {
        if (isEndTile)
        {
            return;
        }
        if (isNotNeeded)
        {
            return;
        }
        if (isSpawnTile)
        {
            return;
        }
        if (!Towermerger.dragging)
        {
            if (turret == null)
            {
                if (correspondingGrid)
                {
                    correspondingGrid.Clear();
                    pathChecker.FindDistance();
                }
                else
                {
                    return;
                }
            }
        }
        else if(Towermerger.dragging && mergeMarking == 1)
        {
            denyIndicator.SetActive(false);
            mouseOverIndicator.SetActive(true);
            return;
        }
        else
        {
            towerMerging.RemoveSecondTile();
        }

        canBuild = 0;
        denyIndicator.SetActive(false);
        mouseOverIndicator.SetActive(false);
        //change colour of tile back to normal
    }

    private void OnMouseDown()
    {
        gettingAS.PlaySound(0);
        if (towerMerging.secondSelect != null)
        {
            towerMerging.secondSelect = null;
            towerMerging.firstSelect.mergeMarking = 0;
        }
        if (canBuild == 0)
        {
            return;
        }
        if (isEndTile)
        {
            Debug.Log("Cannot build on End Tile!");
            return;
        }
        if (isSpawnTile)
        {
            Debug.Log("Cannot build on Start Tile!");
            return;
        }
        if (isNotNeeded)
        {
            return;
        }
        if (canBuild == -1)
        {
            Debug.Log("Will obsturct enemy path, cannot build here");
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        //check for whether any eventsystem (UI elements) is in the way, if yes, tile cannot be selected
        //when there is eventsystem in the scene, it will return an error, so only un-comment it when eventsystem is added

        if (currentTurret != null) //if there is already turret on this tile
        {
            shop.OpenSellMenu(this);
            towerSelection.CurrentSelection(this);
            shop.targetSetting.SetActive(true);
            return;

        }

        if(currentTier == 0)
        {
            shop.OpenBuildMenu(this);
            towerSelection.CurrentSelection(this);
        }
    }

    private GameObject turretPrefab; //store the turret prefab that is spawned on this tile, for upgrading and selling later on

    public void BuildTurret(TurretList turretToBuild)
    {
        if (turretToBuild.turret != null) //if there is a <TurretList> passed in
        {
            bool canBuild = GameManager.instance.MoneyCheck(turretToBuild.cost); //check whether there is enough money

            if (canBuild) //enough money
            {
                gettingAS.PlaySound(1);
                turret = turretToBuild; //store the <TurretList> that was passed in this tile to indicate that a turret is built
                turretPrefab = Instantiate(turret.turret, transform.position, Quaternion.identity); //spawn in the turret
                GameManager.instance.UseMoney(turret.cost); //use money

                currentTurret = turretPrefab.GetComponent<Turret>(); //for add on stats later
                currentTier++;
                correspondingGrid.Block();
                NewAddOn();
            }
            else
            {
                Debug.Log("Not enough money");
            }

            towerSelection.RemoveSelection();
        }
        else
        {
            shop.OpenBuildMenu(this); //if not, open the build menu
        }

    }
    public void DestroyTurret() //reset all variables when the turret is sold
    {
        currentTurret.Unsubscribe();
        denyIndicator.SetActive(false);
        mouseOverIndicator.SetActive(false);
        Destroy(turretPrefab);
        turret = null;
        currentTurret = null;
        currentTier = 0;
        NewAddOn();
        addOn = newAddOn;
        path1Max = false;
        path2Max = false;
        path1UpgradeStatus = 0;
        path2UpgradeStatus = 0;
        path3UpgradeStatus = 0;
        towerSelection.RemoveSelection();

        correspondingGrid.Clear();
    }
    public void UpdateTurretPrefab(GameObject updatedPrefab)
    {
        towerSelection.RemoveSelection();
        upgrading = false;
        currentTurret.Unsubscribe();
        Destroy(turretPrefab);
        turretPrefab = Instantiate(updatedPrefab, transform.position, Quaternion.identity); 
        currentTurret = turretPrefab.GetComponent<Turret>(); //reset the stats to original
    }
    private void Update()
    {
        if (paused)
        {
            return;
        }

        if (isNotNeeded)
        {
            return;
        }

        if (selected && currentTurret != null && Towermerger.dragging)
        {
            mergeRangeIndicator.SetActive(true);
            targetRangeIndicator.SetActive(false);

            MergeRangeIndicatorColor();
        }
        else if(selected && currentTurret != null && !Towermerger.dragging)
        {
            mergeRangeIndicator.SetActive(false);
            targetRangeIndicator.SetActive(true);
        }
        else
        {
            mergeRangeIndicator.SetActive(false);
            targetRangeIndicator.SetActive(false);
        }

        if (currentTurret != null)
        {
            mergeRange = currentTurret.mergeRange;

            mergeRangeIndicator.transform.localScale = Vector3.one * 10 * (mergeRange * 2);
            targetRangeIndicator.transform.localScale = Vector3.one * 10 * (currentTurret.range * 2);

            if (path1UpgradeStatus >= turret.UpgradePath1.Length) //when path1 is at max tier
            {
                path1Max = true;
            }
            if (path2UpgradeStatus >= turret.UpgradePath2.Length) //when path2 is at max tier
            {
                path2Max = true;
            }
            if (path3UpgradeStatus >= turret.UpgradePath3.Length) //when path3 is at max tier
            {
                path3Max = true;
            }

            currentTurret.SetTarget(targetSettingIndex);

            if (currentTurret.currentFill >= currentTurret.gaugeMaxFill && currentTier < currentTurret.maxTier)
            {
                currentTurret.AllowUpgrade();
            }
            else if (currentTier == currentTurret.maxTier)
            {
                currentTurret.MaxTierReached();
            }
        }
        else
        {
            mergeRangeIndicator.SetActive(false);
            targetRangeIndicator.SetActive(false);
        }
    }
    void AddOn() 
    {
        currentTurret.AddOnStats(addOn);
        addOn = newAddOn; //update values of addOn to prevent loop
    }
    void NewAddOn()
    {
        newAddOn.fireRate = 0;
        newAddOn.bulletDamage = 0;

        newAddOn.explosionDamage = 0;
        newAddOn.explosionRadius = 0;

        newAddOn.damageOverTime = 0;
        newAddOn.slowFactor = 0;

        newAddOn.areaDamage = 0;
        newAddOn.burnDamage = 0;
        newAddOn.burnDuration = 0;
        newAddOn.burnRate = 0;
    }
    public void StackAddOn(AddOnTurretStats stack) //add together all the increase in stats
    {
        newAddOn.fireRate += stack.fireRate;
        newAddOn.bulletDamage += stack.bulletDamage;

        newAddOn.explosionDamage += stack.explosionDamage;
        newAddOn.explosionRadius += stack.explosionRadius;

        newAddOn.damageOverTime += stack.damageOverTime;
        newAddOn.slowFactor += stack.slowFactor;

        newAddOn.areaDamage += stack.areaDamage;
        newAddOn.burnDamage += stack.burnDamage;
        newAddOn.burnDuration += stack.burnDuration;
        newAddOn.burnRate += stack.burnRate;
    }

    private void LateUpdate() //run only after all the other codes have done running through, to make sure updating of the turret's stats only happens after the stats are reset
    {
        if (addOn != null && newAddOn != addOn) //add the total increase stats to the reset values
        {
            AddOn();
        }
    }

    //when mouse click is held down
    private void OnMouseDrag()
    {
        if (currentTurret == null)
        {
            return;
        }
        if (currentTurret.CanBeUpgraded)
        {
            if (upgrading)
            {
                shop.ResetShop();
            }
            Towermerger.dragging = true;
            towerMerging.Passingtile(this);
            Debug.Log("dragging tower");
        }
        else
        {
            Debug.Log("Gauge is not full!");
            return;
        }
    }

    private void OnMouseUp()
    {
        if (Towermerger.dragging)
        {
            Towermerger.dragging = false;
            if (towerMerging.secondSelect && towerMerging.secondSelect.canBuild == 1 && towerMerging.mergecheckcheck == true)
            {
                upgrading = true;
                Debug.Log("Upgrading turret");
                //open upgrade menu
                if (currentTurret.CanBeUpgraded && towerMerging.secondSelect.currentTurret)
                {
                    shop.OpenUpgradeMenu(towerMerging.secondSelect.turret, towerMerging.secondSelect);
                }
                else
                {
                    upgrading = false;
                    return;
                }
            }
            else
            {
                upgrading = false;
                towerMerging.ResetScript();
                return;
            }
        }
    }
    public void ResetSelect()
    {
        denyIndicator.SetActive(false);
        mouseOverIndicator.SetActive(false);
    }

    public void MergeRangeIndicatorColor()
    {
        if (Towermerger.dragging)
        {
            bool green = towerMerging.MergeCheck();
            if (green)
            {
                mergeRangeIndicator.GetComponent<Renderer>().material.color = mergingColor;
            }
            else
            {
                mergeRangeIndicator.GetComponent<Renderer>().material.color = denyColor;
            }
        }
        else
        {
            if (currentTurret.currentFill < currentTurret.gaugeMaxFill)
            {
                mergeRangeIndicator.GetComponent<Renderer>().material.color = preparingColor;
            }
            else if (currentTurret.currentFill >= currentTurret.gaugeMaxFill)
            {
                mergeRangeIndicator.GetComponent<Renderer>().material.color = mergingColor;
            }
        }
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(transform.position, mergeRange);
    //}
}
