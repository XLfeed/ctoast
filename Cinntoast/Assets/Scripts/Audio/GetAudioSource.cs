﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetAudioSource : MonoBehaviour
{
    public string[] soundNames;

    [HideInInspector]
    public AudioSource[] parentedSources;
    [HideInInspector]
    public int[] audioIndex;

    // Start is called before the first frame update
    void Start()
    {
        parentedSources = new AudioSource[soundNames.Length];
        audioIndex = new int[soundNames.Length];

        for (int i = 0; i < soundNames.Length; i++)
        {
            AudioSource newSource = AudioManager.instance.GetAudioSource(soundNames[i], this.transform);
            parentedSources[i] = newSource;
            audioIndex[i] = AudioManager.instance.GetSoundIndex(soundNames[i]);
            if(parentedSources[i] != null)
            {
                if (parentedSources[i].playOnAwake)
                {
                    PlaySound(i);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < parentedSources.Length; i++)
        {
            AudioManager.instance.UpdateVolume(parentedSources[i], audioIndex[i]);

            
        }
    }
    public void PlaySound(int SourceIndex)
    {
        if(parentedSources[SourceIndex] != null)
        {
            parentedSources[SourceIndex].Play();
        }
    }
    public void StopSound(int SourceIndex)
    {
        if (parentedSources[SourceIndex] != null)
        {
            parentedSources[SourceIndex].Stop();
        }
    }
}
