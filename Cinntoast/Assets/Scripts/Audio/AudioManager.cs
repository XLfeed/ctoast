﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    [Header("Adjustment")]
    [Range(0f, 1f)]
    public float volume = 0.7f;
    [Range(0f, 1f)]
    public float pitch = 1f;
    [Range(0f, 1f)]
    public float spatialBlend = 0f;
    public bool mute = false;
    public bool playOnAwake = false;
    public bool loop = false;

    public AudioSource source;
}


public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField]
    Sound[] sounds;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one AudioManager in the scene");
        }
        else
        {
            instance = this;
        }
    }

    public AudioSource GetAudioSource(string soundName, Transform parentTransform)
    {
        int soundIndex = -1;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == soundName)
            {
                soundIndex = i;
            }
        }
        if (soundIndex < sounds.Length && soundIndex >= 0)
        {
            GameObject newSource = new GameObject(/*"Sound_" + soundIndex + "_" + */sounds[soundIndex].name);
            newSource.transform.position = parentTransform.position;
            newSource.transform.SetParent(parentTransform);

            AudioSource theSource = newSource.AddComponent<AudioSource>();
            sounds[soundIndex].source = theSource;

            sounds[soundIndex].source.clip = sounds[soundIndex].clip;
            sounds[soundIndex].source.playOnAwake = sounds[soundIndex].playOnAwake;
            sounds[soundIndex].source.loop = sounds[soundIndex].loop;
            sounds[soundIndex].source.mute = sounds[soundIndex].mute;

            sounds[soundIndex].source.volume = sounds[soundIndex].volume;
            sounds[soundIndex].source.pitch = sounds[soundIndex].pitch;
            sounds[soundIndex].source.spatialBlend = sounds[soundIndex].spatialBlend;

            return sounds[soundIndex].source;
        }
        else
        {
            Debug.Log("Sound not Available!");
            return null;
        }
    }
    public int GetSoundIndex(string soundName)
    {
        int soundIndex = -1;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == soundName)
            {
                soundIndex = i;
            }
        }
        if (soundIndex < sounds.Length && soundIndex >= 0)
        {
            return soundIndex;
        }
        else
        {
            return -1;
        }
    }

    public void UpdateVolume(AudioSource source, int audioIndex)
    {
        int soundIndex = 0;
        for (int i = 0; i < sounds.Length; i++)
        {
            if (i == audioIndex)
            {
                soundIndex = i;
            }
        }
        if (sounds[soundIndex] != null && source != null)
        {
            source.mute = sounds[soundIndex].mute;
            source.volume = sounds[soundIndex].volume;
        }

    }
}
