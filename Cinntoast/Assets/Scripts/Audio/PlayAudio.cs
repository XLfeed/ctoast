﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{
    private GetAudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<GetAudioSource>();
        audio.PlaySound(0);
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 1);
    }
}
