﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AddOnTurretStats
{
    [Header("Basic Turret")]
    public float range;
    //public float rotateSpeed;
    public float fireRate;
    public float bulletDamage;

    [Header("Rocket Turret")]
    public float explosionDamage;
    public float explosionRadius;

    [Header("Lazer Turret")]
    public float damageOverTime;
    public float slowFactor;
    //public LineRenderer beam;
    //public ParticleSystem lazerPart;

    [Header("Area Turret")]
    public float areaDamage;
    public float burnDamage;
    public float burnDuration;
    public float burnRate;
}
