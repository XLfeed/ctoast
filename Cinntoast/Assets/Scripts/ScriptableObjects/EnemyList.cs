﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyList
{
    public string name;
    public GameObject enemy;
    public int count;
}
