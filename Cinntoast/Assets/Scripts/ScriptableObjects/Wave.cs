﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave
{
    public string name;
    public float phaseRate;

    [System.Serializable]
    public class Phase
    {
        public string name;
        public EnemyList[] enemies;
        public float spawnRate;
    }

    public Phase[] phases;
}
