﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretUpgradeList
{
    public string name;
    public GameObject upgrade;
    public AddOnTurretStats addOn;
    public int upgradeCost;
}
