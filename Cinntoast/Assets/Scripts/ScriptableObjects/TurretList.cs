﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TurretList
{
    public string name;
    public GameObject turret;
    public int cost;
    public int totalValue;
    public int sellFactor;

    [System.Serializable]
    public class TurretUpgradeList
    {
        public string name;
        public GameObject upgrade;
        public AddOnTurretStats addOn;
        public int upgradeCost;
    }

    public TurretUpgradeList[] UpgradePath1;
    public TurretUpgradeList[] UpgradePath2;
    public TurretUpgradeList[] UpgradePath3;
}
