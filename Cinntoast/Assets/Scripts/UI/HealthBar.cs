﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    void Update()
    {
        // Make sure HealthBar's Canvas faces Player when rotating throughout
        transform.rotation = Camera.main.transform.rotation * Quaternion.Euler(Vector3.up * 180);
    }
}
