﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public float heightOffSet;

    private Node selectedNode;

    private TurretList turretSelected;
    private TurretList turretToUpgrade;

    public bool canUpgrade = false;

    private Animator shopAnim;
    private BuildManager buildManager;

    public Button tier2UpgradeButton1;
    public Button tier2UpgradeButton2;
    public Button tier2UpgradeButton3;

    public Button tier3Path1UpgradeButtonPower;
    public Button tier3Path1UpgradeButtonUtility;
    public Button tier3Path2UpgradeButtonPower;
    public Button tier3Path2UpgradeButtonUtility;
    public Button tier3Path3UpgradeButtonPower;
    public Button tier3Path3UpgradeButtonUtility;

    public Button buildButton;

    public string[] targetText; //4 strings (nearest, strongest, first, last)
    public Text targetTextBox;
    public GameObject targetSetting;

    private Towermerger merger;
    private TowerSelection towerSelection;
    private GameObject[] menus;
    private GameObject activeMenu;

    public GetAudioSource gettingAS;

    private bool tutorial = false;

    public GameObject towerImages;
    private GameObject[] images;

    private void Start()
    {
        shopAnim = GetComponent<Animator>();
        merger = FindObjectOfType<Towermerger>();
        buildManager = FindObjectOfType<BuildManager>();
        towerSelection = FindObjectOfType<TowerSelection>();

        menus = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            menus[i] = transform.GetChild(i).gameObject;
        }

        images = new GameObject[towerImages.transform.childCount];
        for (int i = 0; i < images.Length; i++)
        {
            images[i] = towerImages.transform.GetChild(i).gameObject;
        }
    }

    void Update()
    {
        if(selectedNode != null)
        {
            targetTextBox.text = targetText[selectedNode.targetSettingIndex]; //text to show which enemy the tower is targetting(first, last, nearest, strongest)
        }

        if (Input.GetMouseButtonDown(1)) //right click to deselect everything, and close shop UI
        {
            ResetShop();
        }

        if (selectedNode != null)
        {
            if (buildButton && !selectedNode.currentTurret)
            {
                bool canUpgrade = GameManager.instance.MoneyCheck(buildManager.turretList[0].cost);

                if (canUpgrade)
                {
                    buildButton.interactable = true;
                }
                else
                {
                    buildButton.interactable = false;
                }
            }

            if (selectedNode.currentTurret
                && selectedNode.currentTurret.currentFill >= selectedNode.currentTurret.gaugeMaxFill)
            {
                if(selectedNode.currentTier == 1)
                {
                    if (tier2UpgradeButton1)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath1[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier2UpgradeButton1.interactable = true;
                        }
                        else
                        {
                            tier2UpgradeButton1.interactable = false;
                        }
                    }
                    if (tier2UpgradeButton2)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath2[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier2UpgradeButton2.interactable = true;
                        }
                        else
                        {
                            tier2UpgradeButton2.interactable = false;
                        }
                    }
                    if (tier2UpgradeButton3)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath3[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier2UpgradeButton3.interactable = true;
                        }
                        else
                        {
                            tier2UpgradeButton3.interactable = false;
                        }
                    }
                  
                }//check enable or disable tier 2 buttons

                else if (selectedNode.currentTier == 2 && selectedNode.currentTier < selectedNode.currentTurret.maxTier)
                {
                    if (tier3Path1UpgradeButtonPower)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath1[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path1UpgradeButtonPower.interactable = true;
                        }
                        else
                        {
                            tier3Path1UpgradeButtonPower.interactable = false;
                        }
                    }
                    if (tier3Path2UpgradeButtonPower)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath1[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path2UpgradeButtonPower.interactable = true;
                        }
                        else
                        {
                            tier3Path2UpgradeButtonPower.interactable = false;
                        }
                    }
                    if (tier3Path3UpgradeButtonPower)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath3[selectedNode.currentTier - 1].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path3UpgradeButtonPower.interactable = true;
                        }
                        else
                        {
                            tier3Path3UpgradeButtonPower.interactable = false;
                        }
                    }
                    if (tier3Path1UpgradeButtonUtility)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath1[selectedNode.currentTier].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path1UpgradeButtonUtility.interactable = true;
                        }
                        else
                        {
                            tier3Path1UpgradeButtonUtility.interactable = false;
                        }
                    }
                    if (tier3Path2UpgradeButtonUtility)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath2[selectedNode.currentTier].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path2UpgradeButtonUtility.interactable = true;
                        }
                        else
                        {
                            tier3Path2UpgradeButtonUtility.interactable = false;
                        }
                    }
                    if (tier3Path3UpgradeButtonUtility)
                    {
                        bool canUpgrade = GameManager.instance.MoneyCheck(selectedNode.turret.UpgradePath3[selectedNode.currentTier].upgradeCost);
                        if (canUpgrade)
                        {
                            tier3Path3UpgradeButtonUtility.interactable = true;
                        }
                        else
                        {
                            tier3Path3UpgradeButtonUtility.interactable = false;
                        }
                    }
                }//check enable or disable tier 3 buttons
            }
        }//Compare cost and money, and disable buttons for towers with not enough money to build
        else
        {
            if (buildButton)
            {
                buildButton.interactable = false;
            }
            if (tier2UpgradeButton1)
            {
                tier2UpgradeButton1.interactable = false;
            }
            if (tier2UpgradeButton2)
            {
                tier2UpgradeButton2.interactable = false;
            }
            if (tier2UpgradeButton3)
            {
                tier2UpgradeButton3.interactable = false;
            }
            if (tier3Path1UpgradeButtonPower)
            {
                tier3Path1UpgradeButtonPower.interactable = false;
            }
            if (tier3Path2UpgradeButtonPower)
            {
                tier3Path2UpgradeButtonPower.interactable = false;
            }
            if (tier3Path3UpgradeButtonPower)
            {
                tier3Path3UpgradeButtonPower.interactable = false;
            }
            if (tier3Path1UpgradeButtonUtility)
            {
                tier3Path1UpgradeButtonUtility.interactable = false;
            }
            if (tier3Path2UpgradeButtonUtility)
            {
                tier3Path2UpgradeButtonUtility.interactable = false;
            }
            if (tier3Path3UpgradeButtonUtility)
            {
                tier3Path3UpgradeButtonUtility.interactable = false;
            }
        }//disable all buttons
    }
    public void SelectTurret(int turretIndex) //trigger by a button, assign different turretIndex to different buttons to select different turrets to build
    {
        turretSelected = BuildManager.instance.GetTurretToBuild(turretIndex); //return a <TurretList> of the selected turret and store in a variable

        if (selectedNode != null) //if a tile is selected
        {
            selectedNode.BuildTurret(turretSelected); //build turret and reset the shop
            GameManager.instance.TurretCount(1);
            activeMenu.SetActive(false);
            turretSelected = null;
            selectedNode = null;

            BuildManager.instance.Deselect();
        }
    }

    public void OpenSellMenu(Node node)
    {
        if (activeMenu)
        {
            activeMenu.SetActive(false);
        }

        selectedNode = node;

        if(selectedNode.currentTier >= 1)
        {
            ShowImage();
        }

        turretToUpgrade = selectedNode.turret;
        activeMenu = menus[5];
        transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
        activeMenu.SetActive(true);
    }

    public void OpenBuildMenu(Node node) //when build menu is triggered, pass the selected tile into the script
    {
        if (activeMenu)
        {
            activeMenu.SetActive(false);
        }
        targetSetting.SetActive(false);

        selectedNode = node;

        activeMenu = menus[4];
        transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
        activeMenu.SetActive(true);
    }

    public void OpenUpgradeMenu(TurretList currentTurret, Node node) //when upgrade menu is triggered, the turretList attachedto the tile and the tile itself is passed in
    {
        if (merger.secondSelect.currentTurret && merger.firstSelect.currentTurret.CanBeUpgraded && merger.secondSelect.currentTurret.CanBeUpgraded)
        {
            turretToUpgrade = currentTurret;
            selectedNode = node;

            targetSetting.SetActive(false);

            turretSelected = null;
            BuildManager.instance.Deselect();

            activeMenu.SetActive(false);
            GetMenu();
        }
        else
        {
            merger.firstSelect.ResetSelect();
            merger.ResetScript();
            Debug.Log("Merge Failed!");
        }
    }

    private void GetMenu()
    {
        if(selectedNode.currentTier == 1)
        {
            activeMenu = menus[3];
            transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
            activeMenu.SetActive(true);
        }
        else if(selectedNode.currentTier == 2)
        {
            if(selectedNode.path1UpgradeStatus == 1)
            {
                activeMenu = menus[0];
                transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
                activeMenu.SetActive(true);
            }
            else if(selectedNode.path2UpgradeStatus == 1)
            {
                activeMenu = menus[1];
                transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
                activeMenu.SetActive(true);
            }
            else if (selectedNode.path3UpgradeStatus == 1)
            {
                activeMenu = menus[2];
                transform.position = selectedNode.transform.position + Vector3.up * heightOffSet;
                activeMenu.SetActive(true);
            }
        }
    } //enable the correct menu

    public void UpgradePath1(int upgradeIndex)
    {
        gettingAS.PlaySound(0);
        merger.firstSelect.DestroyTurret();
        merger.ResetScript();

        GameManager.instance.UseMoney(turretToUpgrade.UpgradePath1[upgradeIndex].upgradeCost);
        turretToUpgrade.totalValue += turretToUpgrade.UpgradePath1[upgradeIndex].upgradeCost;

        GameObject newPrefab = turretToUpgrade.UpgradePath1[upgradeIndex].upgrade;
        selectedNode.UpdateTurretPrefab(newPrefab);

        AddOnTurretStats addOn = turretToUpgrade.UpgradePath1[upgradeIndex].addOn;
        selectedNode.StackAddOn(addOn);
        selectedNode.path1UpgradeStatus++;
        selectedNode.currentTier++;

        Debug.Log("Path 1 Status: " + selectedNode.path1UpgradeStatus);
        Debug.Log("Upgraded path 1!!");

        activeMenu.SetActive(false);
        targetSetting.SetActive(false);

        GetTutorial();
    }//upgrade Path 1
    public void UpgradePath2(int upgradeIndex)
    {
        gettingAS.PlaySound(0);
        merger.firstSelect.DestroyTurret();
        merger.ResetScript();

        GameManager.instance.UseMoney(turretToUpgrade.UpgradePath2[upgradeIndex].upgradeCost);
        turretToUpgrade.totalValue += turretToUpgrade.UpgradePath2[upgradeIndex].upgradeCost;

        GameObject newPrefab = turretToUpgrade.UpgradePath2[upgradeIndex].upgrade;
        selectedNode.UpdateTurretPrefab(newPrefab);

        AddOnTurretStats addOn = turretToUpgrade.UpgradePath2[upgradeIndex].addOn;
        selectedNode.StackAddOn(addOn);
        selectedNode.path2UpgradeStatus++;
        selectedNode.currentTier++;

        Debug.Log("Path 2 Status: " + selectedNode.path2UpgradeStatus);
        Debug.Log("Upgraded path 2!!");

        activeMenu.SetActive(false);
        targetSetting.SetActive(false);

        GetTutorial();
    }//upgrade Path 2
    public void UpgradePath3(int upgradeIndex)
    {
        gettingAS.PlaySound(0);
        merger.firstSelect.DestroyTurret();
        merger.ResetScript();

        GameManager.instance.UseMoney(turretToUpgrade.UpgradePath3[upgradeIndex].upgradeCost);
        turretToUpgrade.totalValue += turretToUpgrade.UpgradePath3[upgradeIndex].upgradeCost;

        GameObject newPrefab = turretToUpgrade.UpgradePath3[upgradeIndex].upgrade;
        selectedNode.UpdateTurretPrefab(newPrefab);

        AddOnTurretStats addOn = turretToUpgrade.UpgradePath3[upgradeIndex].addOn;
        selectedNode.StackAddOn(addOn);
        selectedNode.path3UpgradeStatus++;
        selectedNode.currentTier++;

        Debug.Log("Path 3 Status: " + selectedNode.path3UpgradeStatus);
        Debug.Log("Upgraded path 3!!");

        activeMenu.SetActive(false);
        targetSetting.SetActive(false);

        GetTutorial();
    }//upgrade Path 3

    public void Sell()
    {
        int sellprice =  turretToUpgrade.totalValue / turretToUpgrade.sellFactor;
        GameManager.instance.GetMoney(sellprice);

        if(selectedNode.currentTier == 1)
        {
            GameManager.instance.TurretCount(-1);
        }
        if (selectedNode.currentTier == 2)
        {
            GameManager.instance.TurretCount(-2);
        }
        if (selectedNode.currentTier == 3)
        {
            GameManager.instance.TurretCount(-4);
        }

        selectedNode.DestroyTurret();
        activeMenu.SetActive(false);
        targetSetting.SetActive(false);
    }//sell turret on the selected tile
    public void AddTargetIndex()
    {
        if(selectedNode.targetSettingIndex >= targetText.Length - 1)
        {
            selectedNode.targetSettingIndex = 0;
        }
        else
        {
            selectedNode.targetSettingIndex++;
        }
    }//increase targetSettingIndex
    public void MinusTargetIndex()
    {
        if(selectedNode.targetSettingIndex <= 0)
        {
            selectedNode.targetSettingIndex = targetText.Length - 1;
        }
        else
        {
            selectedNode.targetSettingIndex--;
        }
    }//decrease targetSettingIndex

    public void ResetShop()
    {
        if (towerSelection != null)
        {
            towerSelection.RemoveSelection();
            turretSelected = null;
            targetSetting.SetActive(false);

            if (activeMenu != null)
            {
                if (activeMenu.activeInHierarchy)
                {
                    activeMenu.SetActive(false);
                }
            }
         
            

            merger.ResetScript();
            if (selectedNode)
            {
                selectedNode.upgrading = false;
                selectedNode.ResetSelect();
            }
        }
        else
        {
            return;
        }
    }

    public void SoftReset()
    {
        if (activeMenu)
        {
            targetSetting.SetActive(false);
            activeMenu.SetActive(false);
        }

        if (selectedNode)
        {
            selectedNode.upgrading = false;
            selectedNode.ResetSelect();
        }
    }

    public void TutorialCheck()
    {
        tutorial = true;
    }

    public void GetTutorial()
    {
        if (tutorial)
        {
            TutorialManager tutorialManager = FindObjectOfType<TutorialManager>();
            tutorialManager.TriggerTutorial();
            tutorial = false;
        }
    }

    void ShowImage()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].SetActive(false);
        }
        if(selectedNode.currentTier == 1)
        {
            images[0].SetActive(true);
        }
        else if(selectedNode.currentTier == 2)
        {
            if(selectedNode.path1UpgradeStatus == 1)
            {
                images[2].SetActive(true);
            }
            else if (selectedNode.path2UpgradeStatus == 1)
            {
                images[1].SetActive(true);
            }
            else if (selectedNode.path3UpgradeStatus == 1)
            {
                images[3].SetActive(true);
            }
        }
        else if (selectedNode.currentTier == 3)
        {
            if (selectedNode.path1UpgradeStatus == 2)
            {
                images[6].SetActive(true);
            }
            else if (selectedNode.path2UpgradeStatus == 2)
            {
                images[4].SetActive(true);
            }
            else if (selectedNode.path3UpgradeStatus == 2)
            {
                images[8].SetActive(true);
            }
            else if (selectedNode.path1UpgradeStatus == 3)
            {
                images[7].SetActive(true);
            }
            else if (selectedNode.path2UpgradeStatus == 3)
            {
                images[5].SetActive(true);
            }
            else if (selectedNode.path3UpgradeStatus == 3)
            {
                images[9].SetActive(true);
            }
        }
    }
}
