﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FastForward : MonoBehaviour
{
    int fastForwardToggle = 1;

    public GameObject mainCam;
    public float normalCamSpeed;
    CameraController camMove;
    private GetAudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<GetAudioSource>();
        mainCam = GameObject.Find("Main Camera");
        camMove = mainCam.GetComponent<CameraController>();
        normalCamSpeed = camMove.camSpeed;
    }

    public void FastForwardTime()
    {
        switch (fastForwardToggle)
        {
            case 0:
                Time.timeScale = 1.0f;
                fastForwardToggle++;
                camMove.camSpeed = normalCamSpeed;
                audioSource.PlaySound(0);
                break;
            case 1:
                Time.timeScale = 2.5f;
                fastForwardToggle = 0;
                camMove.camSpeed = normalCamSpeed / 3.0f;
                audioSource.PlaySound(1);
                break;
        }
       
    }
}
