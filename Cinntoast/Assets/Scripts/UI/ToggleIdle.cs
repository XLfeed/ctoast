﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleIdle : MonoBehaviour
{
    public Animator anim;
    public bool idle;

    // Update is called once per frame
    void Update()
    {
        if (idle)
        {
            anim.SetBool("Moving", false);
        }
        else
        {
            anim.SetBool("Moving", true);
        }
    }
}
