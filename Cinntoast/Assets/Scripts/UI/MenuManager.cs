﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    private Animator anim;
    private GetAudioSource audioSource;

    public GameObject menuSounds;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        audioSource = menuSounds.GetComponent<GetAudioSource>();

        anim.SetBool("Start", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            anim.SetBool("Start", true);
        }
        if (Input.GetMouseButtonDown(0))
        {
            audioSource.PlaySound(1);
        }
    }
}
