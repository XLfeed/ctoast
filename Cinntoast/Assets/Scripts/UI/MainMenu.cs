﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private MenuManager menuManager;
    private LoadingScreen sceneLoader;

    [Header("RecipeBook")]
    public GameObject coverPage;
    public GameObject recipePage;
    public GameObject equipPage;

    private void Start()
    {
        menuManager = FindObjectOfType<MenuManager>();
        sceneLoader = FindObjectOfType<LoadingScreen>();
    }

    public void MainMenuButton()
    {
        Time.timeScale = 1.0f;
        sceneLoader.LoadOtherScene(0);
    }

    public void PlayButton()
    {
        Time.timeScale = 1.0f;
        sceneLoader.LoadOtherScene(1);
    }

    public void InstructionButton()
    {
        sceneLoader.LoadOtherScene(2);
    }

    public void LeaveGame()
    {
        Application.Quit();
    }

    public void CreditsButton()
    {
        sceneLoader.LoadOtherScene(5);
    }

    public void RecipeButton()
    {
        sceneLoader.LoadOtherScene(3);
    }

    public void OptionsButton()
    {
        sceneLoader.LoadOtherScene(4);
    }

    public void CoverScreen()
    {
        recipePage.SetActive(false);
        equipPage.SetActive(false);
        coverPage.SetActive(true);
    }

    public void RecipeScreen()
    {
        recipePage.SetActive(true);
        coverPage.SetActive(false);

    }

    public void EqipScreen()
    {
        equipPage.SetActive(true);
        coverPage.SetActive(false);
    }

    public void LoadLevel(int levelIndex)
    {
        sceneLoader.LoadLevel(6 + levelIndex);
    }
}
