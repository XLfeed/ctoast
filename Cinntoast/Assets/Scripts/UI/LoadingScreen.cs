﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LoadingScreen : MonoBehaviour
{
    public Animator anim;
    public Image loadingBar;

    private int animIndex;

    private void Awake()
    {
        GameManager manager = FindObjectOfType<GameManager>();

        if (manager)
        {
            animIndex = -2;
        }
        else
        {
            animIndex = 0;
        }
        anim.SetInteger("FadeState", animIndex);
    }

    public void LoadLevel(int sceneIndex)
    {
        animIndex = 2;
        StartCoroutine(LoadScene(sceneIndex));
    }

    public void LoadOtherScene(int sceneIndex)
    {
        animIndex = 1;
        StartCoroutine(LoadScene(sceneIndex));
    }

    IEnumerator LoadScene(int sceneIndex)
    {
        loadingBar.fillAmount = 0;
        anim.SetInteger("FadeState", animIndex);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadingBar.fillAmount = progress;

            yield return null; 
        }
    }
}
