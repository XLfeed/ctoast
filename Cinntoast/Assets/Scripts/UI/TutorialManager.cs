﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TutorialManager : MonoBehaviour
{
    public GameObject[] TutorialSets;
    public GameObject tutorialCanvas;

    public GameObject nextButton;
    public GameObject previousButton;
    public GameObject continueButton;

    private TypeWritingEffect typeWritter;
    private PauseScreen pauseScreen;
    private GameManager gameManager;
    private Shop shop;

    private int tutorialIndex = 0;

    private bool paused = false;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        shop = FindObjectOfType<Shop>();

        gameManager.TutorialCheck();
        shop.TutorialCheck();
    }

    // Start is called before the first frame update
    void Start()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;

        foreach (GameObject TutorialSet in TutorialSets)
        {
            TutorialSet.SetActive(false);
        }
        typeWritter = null;
        TriggerTutorial();
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
    }

    private void Update()
    {
        UpdateButton();
    }

    public void TriggerTutorial()
    {
        //Pause game
        pauseScreen.TogglePause();
        pauseScreen.thePauseScreen.SetActive(false);
        Debug.Log("Game Paused!");

        //Load Canvas
        tutorialCanvas.SetActive(true);

        //Get the correct tutorial
        TutorialSets[tutorialIndex].SetActive(true);
        typeWritter = TutorialSets[tutorialIndex].GetComponent<TypeWritingEffect>();
        typeWritter.StartTutorial();
    }

    public void NextButton()
    {
        typeWritter.NextPage();
    }
    public void PreviousButton()
    {
        typeWritter.PreviousPage();
    }
    public void ResumeButton()
    {
        //Close Tutorial Canvas
        TutorialSets[tutorialIndex].SetActive(false);
        tutorialCanvas.SetActive(false);
        tutorialIndex++;
        typeWritter = null;

        //Rusume Game
        pauseScreen.TogglePause();
    }

    public void UpdateButton()
    {
        if(typeWritter != null)
        {
            if(typeWritter.pageIndex == 0)
            {
                continueButton.SetActive(false);
                nextButton.SetActive(true);
                previousButton.SetActive(false);
            }
            else if(typeWritter.pageIndex >= typeWritter.Pages.transform.childCount - 1)
            {
                continueButton.SetActive(true);
                nextButton.SetActive(false);
                previousButton.SetActive(true);
            }
            else
            {
                continueButton.SetActive(false);
                nextButton.SetActive(true);
                previousButton.SetActive(true);
            }
        }
    }
}
