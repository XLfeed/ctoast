﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Mask))]
[RequireComponent(typeof(ScrollRect))]
public class ScrollSnap2 : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{

    [Tooltip("Set starting page index - starting from 0")]
    public int theStartingPage = 0;
    [Tooltip("Threshold time for fast swipe in seconds")]
    public float thenFastSwipeThresholdTime = 0.3f;
    [Tooltip("Threshold time for fast swipe in (unscaled) pixels")]
    public int thenFastSwipeThresholdDistance = 100;
    [Tooltip("How fast will page lerp to target position")]
    public float theDecelerationRate = 10f;
    [Tooltip("Button to go to the previous page (optional)")]
    public GameObject thePrevButton;
    [Tooltip("Button to go to the next page (optional)")]
    public GameObject theNextButton;
    [Tooltip("Sprite for unselected page (optional)")]
    public Sprite theUnselectedPage;
    [Tooltip("Sprite for selected page (optional)")]
    public Sprite theSelectedPage;
    [Tooltip("Container with page images (optional)")]
    public Transform thePageSelectionIcons;

    // fast swipes should be fast and short. If too long, then it is not fast swipe
    private int thenFastSwipeThresholdMaxLimit;

    private ScrollRect scrollRectComponent;
    private RectTransform scrollRectRect;
    private RectTransform container;

    private bool horizontal;

    // number of pages in container
    private int pageCount;
    private int currentPage;

    // whether lerping is in progress and target lerp position
    private bool lerp;
    private Vector2 lerpTo;

    // target position of every page
    private List<Vector2> pagePositions = new List<Vector2>();

    // in draggging, when dragging started and where it started
    private bool dragging;
    private float timeStamp;
    private Vector2 startPosition;

    // for showing small page icons
    private bool showPageSelection;
    private int previousPageSelectionIndex;
    // container with Image components - one Image for each page
    private List<Image> pageSelectionImages;


    void Start()
    {

        scrollRectComponent = GetComponent<ScrollRect>();
        scrollRectRect = GetComponent<RectTransform>();
        container = scrollRectComponent.content;
        pageCount = container.childCount;

        // is it horizontal or vertical scrollrect
        if (scrollRectComponent.horizontal && !scrollRectComponent.vertical)
        {
            horizontal = true;
        }
        else if (!scrollRectComponent.horizontal && scrollRectComponent.vertical)
        {
            horizontal = false;
        }
        else
        {
            Debug.LogWarning("Confusing setting of horizontal/vertical direction. Default set to horizontal.");
            horizontal = true;
        }

        lerp = false;

        // init
        JustSetPagePositions();
        JustSetPage(theStartingPage);
        TheInitPageSelection();
        JustSetPageSelection(theStartingPage);

        // prev and next buttons
        if (theNextButton)
            theNextButton.GetComponent<Button>().onClick.AddListener(() => { TheNextScreen(); });

        if (thePrevButton)
            thePrevButton.GetComponent<Button>().onClick.AddListener(() => { ThePreviousScreen(); });
    }

    void Update()
    {
        // if moving to target position
        if (lerp)
        {
            // prevent overshooting with values greater than 1
            float decelerate = Mathf.Min(theDecelerationRate * Time.deltaTime, 1f);
            container.anchoredPosition = Vector2.Lerp(container.anchoredPosition, lerpTo, decelerate);
            // time to stop lerping?
            if (Vector2.SqrMagnitude(container.anchoredPosition - lerpTo) < 0.25f)
            {
                // snap to target and stop lerping
                container.anchoredPosition = lerpTo;
                lerp = false;
                // clear also any scrollrect move that may interfere with our lerping
                scrollRectComponent.velocity = Vector2.zero;
            }

            // switches selection icon exactly to correct page
            if (showPageSelection)
            {
                JustSetPageSelection(JustGetNearestPage());
            }
        }
    }

    private void JustSetPagePositions()
    {
        int width = 0;
        int height = 0;
        int offsetX = 0;
        int offsetY = 0;
        int containerWidth = 0;
        int containerHeight = 0;

        if (horizontal)
        {
            // screen width in pixels of scrollrect window
            width = (int)scrollRectRect.rect.width;
            // center position of all pages
            offsetX = width / 2;
            // total width
            containerWidth = width * pageCount;
            // limit fast swipe length - beyond this length it is fast swipe no more
            thenFastSwipeThresholdMaxLimit = width;
        }
        else
        {
            height = (int)scrollRectRect.rect.height;
            offsetY = height / 2;
            containerHeight = height * pageCount;
            thenFastSwipeThresholdMaxLimit = height;
        }

        // set width of container
        Vector2 newSize = new Vector2(containerWidth, containerHeight);
        container.sizeDelta = newSize;
        Vector2 newPosition = new Vector2(containerWidth / 2, containerHeight / 2);
        container.anchoredPosition = newPosition;

        // delete any previous settings
        pagePositions.Clear();

        // iterate through all container childern and set their positions
        for (int i = 0; i < pageCount; i++)
        {
            RectTransform child = container.GetChild(i).GetComponent<RectTransform>();
            Vector2 childPosition;
            if (horizontal)
            {
                childPosition = new Vector2(i * width - containerWidth / 2 + offsetX, 0f);
            }
            else
            {
                childPosition = new Vector2(0f, -(i * height - containerHeight / 2 + offsetY));
            }
            child.anchoredPosition = childPosition;
            pagePositions.Add(-childPosition);
        }
    }

    private void JustSetPage(int aPageIndex)
    {
        aPageIndex = Mathf.Clamp(aPageIndex, 0, pageCount - 1);
        container.anchoredPosition = pagePositions[aPageIndex];
        currentPage = aPageIndex;

        if (currentPage == 0)
        {
            thePrevButton.SetActive(false);
        }
    }

    private void JustLerpToPage(int aPageIndex)
    {
        aPageIndex = Mathf.Clamp(aPageIndex, 0, pageCount - 1);
        lerpTo = pagePositions[aPageIndex];
        lerp = true;
        currentPage = aPageIndex;

        if (currentPage == 0)
        {
            thePrevButton.SetActive(false);
            theNextButton.SetActive(true);
        }
        else if (currentPage == 1)
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(true);
        }

        else if (currentPage == 2)
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(true);
        }
        
        else if (currentPage == 3)
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(true);
        }
        else if (currentPage == 4)
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(true);
        }

        else if (currentPage == 5)
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(true);
        }

        else
        {
            thePrevButton.SetActive(true);
            theNextButton.SetActive(false);
        }
    }

    private void TheInitPageSelection()
    {
        // page selection - only if defined sprites for selection icons
        showPageSelection = theUnselectedPage != null && theSelectedPage != null;
        if (showPageSelection)
        {
            // also container with selection images must be defined and must have exatly the same amount of items as pages container
            if (thePageSelectionIcons == null || thePageSelectionIcons.childCount != pageCount)
            {
                Debug.LogWarning("Different count of pages and selection icons - will not show page selection");
                showPageSelection = false;
            }
            else
            {
                previousPageSelectionIndex = -1;
                pageSelectionImages = new List<Image>();

                // cache all Image components into list
                for (int i = 0; i < thePageSelectionIcons.childCount; i++)
                {
                    Image image = thePageSelectionIcons.GetChild(i).GetComponent<Image>();
                    if (image == null)
                    {
                        Debug.LogWarning("Page selection icon at position " + i + " is missing Image component");
                    }
                    pageSelectionImages.Add(image);
                }
            }
        }
    }

    private void JustSetPageSelection(int aPageIndex)
    {
        // nothing to change
        if (previousPageSelectionIndex == aPageIndex)
        {
            return;
        }

        // unselect old
        if (previousPageSelectionIndex >= 0)
        {
            pageSelectionImages[previousPageSelectionIndex].sprite = theUnselectedPage;
            pageSelectionImages[previousPageSelectionIndex].SetNativeSize();
        }

        // select new
        pageSelectionImages[aPageIndex].sprite = theSelectedPage;
        pageSelectionImages[aPageIndex].SetNativeSize();

        previousPageSelectionIndex = aPageIndex;
    }

    private void TheNextScreen()
    {
        JustLerpToPage(currentPage + 1);
    }

    private void ThePreviousScreen()
    {
        JustLerpToPage(currentPage - 1);
    }

    private int JustGetNearestPage()
    {
        // based on distance from current position, find nearest page
        Vector2 currentPosition = container.anchoredPosition;

        float distance = float.MaxValue;
        int nearestPage = currentPage;

        for (int i = 0; i < pagePositions.Count; i++)
        {
            float testDist = Vector2.SqrMagnitude(currentPosition - pagePositions[i]);
            if (testDist < distance)
            {
                distance = testDist;
                nearestPage = i;
            }
        }

        return nearestPage;
    }

    public void OnBeginDrag(PointerEventData aEventData)
    {
        // if currently lerping, then stop it as user is draging
        lerp = false;
        // not dragging yet
        dragging = false;
    }

    public void OnEndDrag(PointerEventData aEventData)
    {
        // how much was container's content dragged
        float difference;
        if (horizontal)
        {
            difference = startPosition.x - container.anchoredPosition.x;
        }
        else
        {
            difference = -(startPosition.y - container.anchoredPosition.y);
        }

        // test for fast swipe - swipe that moves only +/-1 item
        if (Time.unscaledTime - timeStamp < thenFastSwipeThresholdTime &&
            Mathf.Abs(difference) > thenFastSwipeThresholdDistance &&
            Mathf.Abs(difference) < thenFastSwipeThresholdMaxLimit)
        {
            if (difference > 0)
            {
                TheNextScreen();
            }
            else
            {
                ThePreviousScreen();
            }
        }
        else
        {
            // if not fast time, look to which page we got to
            JustLerpToPage(JustGetNearestPage());
        }

        dragging = false;
    }

    public void OnDrag(PointerEventData aEventData)
    {
        if (!dragging)
        {
            // dragging started
            dragging = true;
            // save time - unscaled so pausing with Time.scale should not affect it
            timeStamp = Time.unscaledTime;
            // save current position of cointainer
            startPosition = container.anchoredPosition;
        }
        else
        {
            if (showPageSelection)
            {
                JustSetPageSelection(JustGetNearestPage());
            }
        }
    }
}
