﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpButton : MonoBehaviour
{
    public GameObject HelpMenu;

    public void HelpMe()
    {
        HelpMenu.gameObject.SetActive(true);
    }

    public void EndHelp()
    {
        HelpMenu.gameObject.SetActive(false);
    }
}
