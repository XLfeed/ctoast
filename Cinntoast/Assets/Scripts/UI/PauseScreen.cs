﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    public GameObject thePauseScreen;
    public GameObject uiCanvas;

    private TutorialManager tutorial;
    public GameObject tutorialCanvas;

    public event EventHandler<OnEscPressedEventArgs> OnEscPressed;
    public class OnEscPressedEventArgs : EventArgs
    {
        public bool paused;
    }

    private bool gamePaused = false;

    private void Awake()
    {
        tutorial = FindObjectOfType<TutorialManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorial != null && tutorialCanvas.activeInHierarchy)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && thePauseScreen)
        {
            TogglePause();
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
    
    public void TogglePause()
    {
        gamePaused = !gamePaused;
        Debug.Log("Paused?" + gamePaused);
        thePauseScreen.SetActive(gamePaused);
        uiCanvas.SetActive(!gamePaused);
        OnEscPressed?.Invoke(this, new OnEscPressedEventArgs { paused = gamePaused });
    }
}
