﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class TypeWritingEffect : MonoBehaviour
{
    public float delay = 0.1f;
    public GameObject Pages;
    public Text currentTextBox;
    public string[] currentText;
    public GameObject[] currentPage;

    [HideInInspector]
    public int pageIndex;


    IEnumerator ShowText()
    {
        currentPage[pageIndex].SetActive(true);
        if (currentPage[pageIndex].GetComponent<VideoPlayer>())
        {
            currentPage[pageIndex].GetComponent<VideoPlayer>().Play();
        }

        for(int i = 0; i < currentText[pageIndex].Length + 1; i++)
        {
            string textToType = currentText[pageIndex].Substring(0, i);
            currentTextBox.text = textToType;
            yield return new WaitForSeconds(delay);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StopAllCoroutines();
            currentTextBox.text = currentText[pageIndex];
        }
    }

    public void NextPage()
    {
        currentPage[pageIndex].SetActive(false);
        if (currentPage[pageIndex].GetComponent<VideoPlayer>())
        {
            currentPage[pageIndex].GetComponent<VideoPlayer>().Stop();
        }
        pageIndex++;
        StartCoroutine(ShowText());
    }
    public void PreviousPage()
    {
        currentPage[pageIndex].SetActive(false);
        if (currentPage[pageIndex].GetComponent<VideoPlayer>())
        {
            currentPage[pageIndex].GetComponent<VideoPlayer>().Stop();
        }
        pageIndex--;
        StartCoroutine(ShowText());
    }
    public void StartTutorial()
    {
        StartCoroutine(ShowText());
    }
}
