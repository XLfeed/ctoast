﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private PauseScreen pauseScreen;
    private bool paused;

    private Camera cam;
    private float targetZoom;
    private float targetRotation;
    
    public float camSpeed = 40f;
    public float panBorder = 10f;

    public float smoothSpeed = 1f;

    public float zoomSpeed = 3f;
    public float zoomLerpSpeed = 10f;

    public float rotateSpeed;

   [Header("Zoom Limits")]
    public float minZoom = 1.5f;
    public float maxZoom = 5.0f;

    [Header("Z Position")]
    public float minZ;
    public float maxZ;

    [Header("X Position")]
    public float minX;
    public float maxX;

    private void Awake()
    {
        pauseScreen = FindObjectOfType<PauseScreen>();
        pauseScreen.OnEscPressed += TogglePause;
    }

    void TogglePause(object pauseScreen, PauseScreen.OnEscPressedEventArgs e)
    {
        paused = e.paused;
    }

    void Start()
    {
        cam = Camera.main;
        targetZoom = cam.transform.position.y;
    }

    void Update()
    {
        if (paused)
        {
            return;
        }

        EdgeScroll();
        CamZoom();
    }

    void EdgeScroll()
    {
        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - panBorder)
        {
            Vector3 camPos = transform.position;
            camPos.z -= camSpeed * Time.deltaTime;
            camPos.z = Mathf.Clamp(camPos.z, minZ, maxZ);

            Vector3 smoothPos = Vector3.Lerp(transform.position, camPos, smoothSpeed);

            transform.position = smoothPos;
        }
        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= panBorder)
        {

            Vector3 camPos = transform.position;
            camPos.z += camSpeed * Time.deltaTime;
            camPos.z = Mathf.Clamp(camPos.z, minZ, maxZ);

            Vector3 smoothPos = Vector3.Lerp(transform.position, camPos, smoothSpeed);

            transform.position = smoothPos;
        }
        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - panBorder)
        {

            Vector3 camPos = transform.position;
            camPos.x += camSpeed * Time.deltaTime;
            camPos.x = Mathf.Clamp(camPos.x, minX, maxX);

            Vector3 smoothPos = Vector3.Lerp(transform.position, camPos, smoothSpeed);

            transform.position = smoothPos;
        }
        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= panBorder)
        {

            Vector3 camPos = transform.position;
            camPos.x -= camSpeed * Time.deltaTime;
            camPos.x = Mathf.Clamp(camPos.x, minX, maxX);

            Vector3 smoothPos = Vector3.Lerp(transform.position, camPos, smoothSpeed);

            transform.position = smoothPos;
        }
    }

    void CamZoom()
    {
        float amountToZoom = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        targetZoom -= amountToZoom;
        targetZoom = Mathf.Clamp(targetZoom, minZoom, maxZoom);
        float smoothZoom = Mathf.Lerp(cam.transform.position.y, targetZoom, Time.deltaTime * zoomLerpSpeed);
        cam.transform.position = new Vector3(transform.position.x, smoothZoom, transform.position.z);
    }
}
